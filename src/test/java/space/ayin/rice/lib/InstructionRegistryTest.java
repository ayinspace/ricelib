package space.ayin.rice.lib;

import org.junit.Test;
import space.ayin.rice.lib.exceptions.IdProviderCycleException;
import space.ayin.rice.lib.exceptions.RiceInstructionIdCollisionException;
import space.ayin.rice.lib.exceptions.RiceInstructionIdNotFound;
import space.ayin.rice.lib.exceptions.RiceServiceInstanceNotFound;
import space.ayin.rice.lib.test.Class1;
import space.ayin.rice.lib.test.Class2;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class InstructionRegistryTest {
    @Test
    public void scanPackage() throws RiceInstructionIdNotFound, RiceServiceInstanceNotFound, InvocationTargetException, IllegalAccessException, RiceInstructionIdCollisionException, IdProviderCycleException {
        SimpleServiceLocator simpleServiceLocator = new SimpleServiceLocator();
        List<InstructionIdProvider> instructionIdProviders = new ArrayList<>();
        instructionIdProviders.add(new AnnotationIdProvider());
        CompositeIdProvider compositeIdProvider = new CompositeIdProvider(instructionIdProviders);

        simpleServiceLocator.registerServiceInstance(new Class1());
        simpleServiceLocator.registerServiceInstance(new Class2());

        InstructionRegistry instructionRegistry = new InstructionRegistry(simpleServiceLocator, compositeIdProvider);
        instructionRegistry.scanPackage("space.ayin.rice.lib.test");
    }
}