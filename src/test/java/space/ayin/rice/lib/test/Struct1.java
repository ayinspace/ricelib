package space.ayin.rice.lib.test;

import space.ayin.rice.typemodel.RiceStruct;

public class Struct1 extends RiceStruct {
    public int i = 3;

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }
}
