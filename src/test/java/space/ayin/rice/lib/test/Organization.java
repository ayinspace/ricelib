package space.ayin.rice.lib.test;

import space.ayin.rice.typemodel.RiceStruct;

/**
 * Created by nesferatos on 01.09.2017.
 */
public class Organization extends RiceStruct {

    private String name; //3

    private Employee owner; //4

    private Person slave; //5

    private Employee[][] employees; //1

    private String[] titles;

    private int[][] data;

    private boolean legal = false; //2

    private boolean bankrupt; //0

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getOwner() {
        return owner;
    }

    public void setOwner(Employee owner) {
        this.owner = owner;
    }

    public boolean isLegal() {
        return legal;
    }

    public void setLegal(boolean legal) {
        this.legal = legal;
    }

    public boolean getBankrupt() {
        return bankrupt;
    }

    public void setBankrupt(boolean bankrupt) {
        this.bankrupt = bankrupt;
    }

    public Employee[][] getEmployees() {
        return employees;
    }

    public void setEmployees(Employee[][] employees) {
        this.employees = employees;
    }
}
