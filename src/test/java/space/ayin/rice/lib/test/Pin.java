package space.ayin.rice.lib.test;

import space.ayin.rice.typemodel.RiceStruct;

/**
 * Created by nesferatos on 18.09.2017.
 */
public class Pin extends RiceStruct {

    public enum PinState {LOW, HI, HIZ};

    private int pinNum;
    private PinState state;

    public int getPinNum() {
        return pinNum;
    }

    public void setPinNum(int pinNum) {
        this.pinNum = pinNum;
    }

    public PinState getState() {
        return state;
    }

    public void setState(PinState state) {
        this.state = state;
    }
}
