package space.ayin.rice.lib.test;

import space.ayin.rice.lib.Instruction;
import space.ayin.rice.lib.InstructionId;

/**
 * Created by nesferatos on 03.08.2017.
 */
public interface Interface1 {
    @Instruction()
    @InstructionId(id = 1)
    long method3(long i);
}
