package space.ayin.rice.lib.test;

import space.ayin.rice.typemodel.RiceInstructionMethod;
import space.ayin.rice.typemodel.RiceStruct;

public abstract class SampleService extends RiceStruct {

    @RiceInstructionMethod
    public abstract int sum(int x, int y);
}
