package space.ayin.rice.lib.test;

/**
 * Created by nesferatos on 01.09.2017.
 */
public class Employee extends Person {
    private String positionName;
    private DayOfWeek favoriteDay;


    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public DayOfWeek getFavoriteDay() {
        return favoriteDay;
    }

    public void setFavoriteDay(DayOfWeek favoriteDay) {
        this.favoriteDay = favoriteDay;
    }
}
