package space.ayin.rice.lib.test;

import space.ayin.rice.lib.Instruction;
import space.ayin.rice.lib.InstructionId;
import space.ayin.rice.lib.Service;

import java.util.*;

/**
 * Created by nesferatos on 03.08.2017.
 */
@Service
public class Class2 implements Interface1 {

    public void method1() {

    }

    @Instruction()
    @InstructionId(id = 0)
    public long method2() {
        System.out.println(getClass() + " method2");
        return 22;
    }

    @Override
    public long method3(long i) {
        System.out.println(getClass() + " method3 i=" + i);
        return i + 33;
    }

    @Instruction
    @InstructionId(id = 2)
    public long method4(Long i, long j) {
        System.out.println(getClass() + " method4 i=" + i + " j=" + j);
        return i + 44;
    }

    @Instruction
    @InstructionId(id = 3)
    public long method5(long i, Struct1 struct1) {
        System.out.println(getClass() + " method5 " + struct1.i);
        return struct1.i + i;
    }

    @Instruction
    @InstructionId(id = 4)
    public void method6(Organization organization) {
        System.out.println(getClass() + " method6");
        System.out.println("organization: " + organization.getName());
        System.out.println("owner: " + organization.getOwner().getName() + ", age: " + organization.getOwner().getAge());
        System.out.println("bankrupt : " + organization.getBankrupt() + ", legal : " + organization.isLegal());
    }

    @Instruction
    @InstructionId(id = 5)
    public Organization method7(Organization organization) {
        System.out.println(getClass() + " method7");
        System.out.println("organization: " + organization.getName());
        System.out.println("owner: " + organization.getOwner().getName() + ", age: " + organization.getOwner().getAge());
        System.out.println("bankrupt : " + organization.getBankrupt() + ", legal : " + organization.isLegal());

        organization.getOwner().setName("Mr. " + organization.getOwner().getName());
        return organization;
    }


    @Instruction
    @InstructionId(id = 6)
    public Organization[] method8(Organization[] organizations, boolean[] exclude) {
        System.out.println(getClass() + " method8");
        List<Organization> res = new ArrayList<>();
        for (int i = 0; i < organizations.length; i++) {
            Organization organization = organizations[i];
            System.out.println("organization " + i + ": " + organization.getName());
            System.out.println("owner: " + organization.getOwner().getName() + ", age: " + organization.getOwner().getAge());
            System.out.println("bankrupt : " + organization.getBankrupt() + ", legal : " + organization.isLegal());
            organization.getOwner().setName("Mr. " + organization.getOwner().getName());
            if (!exclude[i]) {
                res.add(organization);
            }
        }
        Collections.sort(res, Comparator.comparing(Organization::getName));

        return (Organization[]) res.toArray(new Organization[res.size()]);
    }

    @Instruction()
    @InstructionId(id = 7)
    public Pin method9(Pin pin, Pin.PinState state) {
        System.out.println(getClass() + " method9");
        System.out.println("pin " + pin.getPinNum() + " was " + pin.getState());
        System.out.println("setting pin to " + state);
        pin.setState(state);
        return pin;
    }

}
