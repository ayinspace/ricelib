package space.ayin.rice.lib.test;

import space.ayin.rice.typemodel.RiceStruct;
import space.ayin.rice.typemodel.RiceType;

public class Person extends RiceStruct {

    private int age;

    private String name;

    private boolean alive;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }
}
