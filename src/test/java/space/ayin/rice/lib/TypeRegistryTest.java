package space.ayin.rice.lib;

import org.junit.Test;
import space.ayin.rice.lib.test.Employee;
import space.ayin.rice.lib.test.Person;
import space.ayin.rice.typemodel.BaseTypeRegistry;
import space.ayin.rice.typemodel.RiceStruct;
import space.ayin.rice.typemodel.RiceType;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by nesferatos on 01.10.2017.
 */
public class TypeRegistryTest {

    @Test
    public void registerStruct() throws IOException {
        BaseTypeRegistry typeRegistry = new BaseTypeRegistry();
        RiceType riceStructRiceType = typeRegistry.getOrRegisterJavaType(RiceStruct.class);
        assertEquals(typeRegistry.getChildrenQuantity(riceStructRiceType), 5);

        RiceType riceTypeRiceType = typeRegistry.getOrRegisterJavaType(RiceType.class);
        assertEquals(typeRegistry.getRelativeRiceTypeIndex(riceStructRiceType, riceTypeRiceType) , 2);

        RiceType personRiceType = typeRegistry.getOrRegisterJavaType(Person.class);
        RiceType employeeRiceType = typeRegistry.getOrRegisterJavaType(Employee.class);
        assertEquals(typeRegistry.getChildrenQuantity(riceStructRiceType), 7);
        assertEquals(typeRegistry.getRelativeRiceTypeIndex(riceStructRiceType, riceTypeRiceType) , 4);

        System.out.println("");
    }

}
