package space.ayin.rice.codegen;

import org.junit.Test;
import space.ayin.rice.lib.test.*;
import space.ayin.rice.typemodel.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CodeGenTest {
    @Test
    public void genForType() throws Exception {
        CodeGen codeGen = new CodeGen();
        BaseTypeRegistry baseTypeRegistry = new BaseTypeRegistry();


        List<RiceType> riceTypes = new ArrayList<RiceType>();

//        riceTypes.add(riceVM.getTypeRegistry().getOrRegisterJavaType(SampleService.class));
        riceTypes.add(baseTypeRegistry.getOrRegisterJavaType(Organization.class));
        riceTypes.add(baseTypeRegistry.getOrRegisterJavaType(Employee.class));
        riceTypes.add(baseTypeRegistry.getOrRegisterJavaType(Person.class));
        riceTypes.add(baseTypeRegistry.getOrRegisterJavaType(DayOfWeek.class));
        riceTypes.add(baseTypeRegistry.getOrRegisterJavaType(RiceDebugMessage.class));

        for (RiceType riceType : riceTypes) {
            baseTypeRegistry.unregisterClassForRiceType(riceType);
        }

        //String genCodePackage = "space.ayin.rice.codegen.test.gen";
        String genCodePackage = "codegen.space.ayin.rice.lib.test";

        //String basePackage = "pace.ayin.rice.lib.test";


        String path = "target/generated-sources/java/rice/" + genCodePackage.replace(".", "/") + "/";

        String outputDir = "target/classes";

        BaseMapper mapper = baseTypeRegistry.createAndRegisterNecessaryStructClasses(path, outputDir, genCodePackage);

        System.out.println();


        /*Class<?> mapperClass = classLoader.loadClass(genCodePackage + ".Mapper");
        BaseMapper mapperInstance = (BaseMapper) mapperClass.getConstructor(BaseTypeRegistry.class).newInstance(new BaseTypeRegistry());


            Class<?> employeeClass = classLoader.loadClass(genCodePackage + ".Employee");
            Class<?> personClass = classLoader.loadClass(genCodePackage + ".Person");
            Class<?> organizationClass = classLoader.loadClass(genCodePackage + ".Organization");
            Class<?> employeeListClass = Array.newInstance(employeeClass, 3, 4).getClass();
            Class<?> dayOfWeekClass = classLoader.loadClass(genCodePackage+".DayOfWeek");
        Class<?> employeeClass = classLoader.loadClass(genCodePackage + ".Employee");
        Class<?> personClass = classLoader.loadClass(genCodePackage + ".Person");
        Class<?> organizationClass = classLoader.loadClass(genCodePackage + ".Organization");
        Class<?> employeeListClass = Array.newInstance(employeeClass, 3).getClass();
        Class<?> dayOfWeekClass = classLoader.loadClass(genCodePackage + ".DayOfWeek");

        mapperInstance.getTypeRegistry().getOrRegisterJavaType(employeeClass);
        mapperInstance.getTypeRegistry().getOrRegisterJavaType(organizationClass);
//            mapperInstance.getTypeRegistry().getOrRegisterJavaType(personClass);


        RiceStruct employeeInstance = (RiceStruct) employeeClass.newInstance();
        RiceStruct employeeInstance2 = (RiceStruct) employeeClass.newInstance();

        String testEmployeeName = "employee1";
        int testEmployeeAge = 23;

        String testEmployeeName2 = "employee500";
        int testEmployeeAge2 = 9000;


        employeeClass.getMethod("setName", new Class[]{String.class})
                .invoke(employeeInstance, testEmployeeName);

        employeeClass.getMethod("setAge", new Class[]{int.class})
                .invoke(employeeInstance, testEmployeeAge);

        employeeClass.getMethod("setAlive", new Class[]{boolean.class})
                .invoke(employeeInstance, true);

        employeeClass.getMethod("setPositionName", new Class[]{String.class})
                .invoke(employeeInstance, "worker");

        employeeClass.getMethod("setFavoriteDay", new Class[]{dayOfWeekClass})
                .invoke(employeeInstance, dayOfWeekClass.getEnumConstants()[6]);

        Object employeeName = employeeClass.getMethod("getName", new Class[]{})
                .invoke(employeeInstance, null);

        employeeClass.getMethod("setName", new Class[]{String.class})
                .invoke(employeeInstance2, testEmployeeName2);

        employeeClass.getMethod("setAge", new Class[]{int.class})
                .invoke(employeeInstance2, testEmployeeAge2);

        employeeClass.getMethod("setAlive", new Class[]{boolean.class})
                .invoke(employeeInstance2, false);

        employeeClass.getMethod("setPositionName", new Class[]{String.class})
                .invoke(employeeInstance2, "god");

        employeeClass.getMethod("setFavoriteDay", new Class[]{dayOfWeekClass})
                .invoke(employeeInstance2, dayOfWeekClass.getEnumConstants()[2]);

        Object employeeName2 = employeeClass.getMethod("getName", new Class[]{})
                .invoke(employeeInstance2, null);

        assertEquals(testEmployeeName, employeeName);
        assertEquals(testEmployeeName2, employeeName2);


        RicePacket mappedOrganization;// = mapperInstance.serialize(employeeInstance);

        assertEquals(testEmployeeName, employeeName);

            Object employees1 = Array.newInstance(employeeClass, 3);
            Object employees2 = Array.newInstance(employeeClass, 3);
            Array.set(employees1, 0 , employeeInstance);
            Array.set(employees1, 1 , null);
            Array.set(employees1, 2 , employeeInstance2);
            Array.set(employees2, 0 , null);
            Array.set(employees2, 1 , null);
            Array.set(employees2, 2 , employeeInstance);
            Object employees = Array.newInstance(employeeClass, 2, 3);
            Array.set(employees, 0, employees1);
            Array.set(employees, 1, employees2);

        Object employees = Array.newInstance(employeeClass, 3);
        Array.set(employees, 0, employeeInstance);
        Array.set(employees, 1, null);
        Array.set(employees, 2, employeeInstance2);

        RiceStruct organizationInstance = (RiceStruct) organizationClass.newInstance();

        organizationClass.getMethod("setOwner", new Class[]{employeeClass})
                .invoke(organizationInstance, employeeInstance);

        organizationClass.getMethod("setSlave", new Class[]{personClass})
                .invoke(organizationInstance, employeeInstance);

        organizationClass.getMethod("setEmployees", new Class[]{employeeListClass})
                .invoke(organizationInstance, employees);

        mappedOrganization = mapperInstance.mapToPacket(organizationInstance);

        byte[] serializerOrganization = mappedOrganization.toByteArray();

        List<RiceStruct> structs = mapperInstance.deserialize(serializerOrganization);
        //(RicePacket ricePacket, RiceType riceType)

//            RiceType organizationType = mapperInstance.getTypeRegistry().getOrRegisterJavaType(organizationClass);
//            Method deserialize = mapperClass.getMethod("deserialize"+organizationType.getName(), RicePacket.class);
//            deserialize.setAccessible(true);
//            RiceStruct deserialized = (RiceStruct) deserialize.invoke(mapperInstance, mappedOrganization);

        assertEquals(structs.get(1), organizationInstance);
        System.out.println(organizationInstance);
        System.out.println(structs.get(1));

        */

    }


}
