package space.ayin.rice.mapper;

import com.ubs_soft.commons.collections.BitIterator;
import org.junit.Test;
import space.ayin.rice.model.RiceCons;
import space.ayin.rice.typemodel.*;
import space.ayin.rice.vm.RiceVM;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by nesferatos on 28.09.2017.
 */
public class BaseMapperTest {

    public BaseMapperTest() {
//        super(new BaseTypeRegistry());
    }


    protected void deserialize(BitIterator iterator, List<RiceStruct> result) throws ClassNotRegisteredException {

    }




//    @Test
//    public void testAppendValue() throws MappingException {
//        RiceCons head = new RiceCons();
//        RiceCons cons = head;
//        for (byte i = 0; i < 10; i++) {
//            System.out.println("appending " + i);
//            cons = appendbyte(i, cons);
//        }
//        System.out.println(head);
//    }
//
//    @Test
//    public void testPrimitiveValues() throws ArgumentMappingException, MappingException {
//
//        RiceCons head = new RiceCons();
//
//        RiceCons cursor = head;
//
//        cursor = appendboolean(true, cursor);//offset for cursor
//
//        cursor = appendint(23, cursor);
//
//        cursor = appendint(234, cursor);
//
//        int a1 = retrieveint(head);
//        int a2 = retrieveint(head);
//
//        assertEquals(23, a1);
//        assertEquals(234, a2);
//
//        cursor = appendString("abc 123", cursor);
//
//        String s = retrieveString(head);
//        assertEquals("abc 123", s);
//
//    }

    /*@Test
    public void testEnumValues() throws ArgumentMappingException, MappingException {

        RiceValue pinValue = riceVM.getMapper().mapToRiceValue(Pin.PinState.HI);

        Pin.PinState pinState = riceVM.getMapper().mapRiceValue(pinValue, Pin.PinState.class);

        assertEquals(Pin.PinState.HI, pinState);

    }*/

    /*@Test
    public void testStructValues() throws ArgumentMappingException, MappingException {
        Employee employee = new Employee();
        employee.setName("qweqwe");
        employee.setAge(22);

        RiceCons employeeCons = riceVM.getMapper().mapToRiceCons(employee);

        System.out.println(employeeCons);


        /*RiceTuple employeeTupleValue = new RiceTuple();
        employeeTupleValue.getValues().add(new RiceI32Value(18));
        employeeTupleValue.getValues().add(new RiceStringValue("Aaron Aaronson"));

        Employee employee = BaseMapper.mapRiceValue(employeeTupleValue, Employee.class);
        assertEquals("Aaron Aaronson", employee.getName());
        assertEquals(18, employee.getAge());

        assertEquals(employeeTupleValue, mapper.mapValue(employee));

        Employee employee1 = new Employee();
        employee1.setName("Aaron Aaronson");
        employee1.setAge(19);

        assertNotEquals(employeeTupleValue, mapper.mapValue(employee1));

        RiceTuple organizationTupleValue = new RiceTuple();
        organizationTupleValue.getValues().add(new RiceBooleanValue(true));

        RiceTuple employeesTupleValue = new RiceTuple();
        employeesTupleValue.getValues().add(new RiceI32Value(2));//array size
        employeesTupleValue.getValues().add(employeeTupleValue);
        RiceTuple anotherEmployeeTupleValue = new RiceTuple();
        anotherEmployeeTupleValue .getValues().add(new RiceI32Value(22));
        anotherEmployeeTupleValue .getValues().add(new RiceStringValue("Louis Jordan"));
        employeesTupleValue.getValues().add(anotherEmployeeTupleValue);

        organizationTupleValue.getValues().add(employeesTupleValue);


        organizationTupleValue.getValues().add(new RiceBooleanValue(false));
        organizationTupleValue.getValues().add(new RiceStringValue("UBS"));
        organizationTupleValue.getValues().add(employeeTupleValue);


        Organization organization = BaseMapper.mapRiceValue(organizationTupleValue, Organization.class);

        assertEquals("UBS", organization.getName());

        assertEquals("Aaron Aaronson", organization.getOwner().getName());

        assertEquals(organizationTupleValue, mapper.mapValue(organization));

    }*/


}
