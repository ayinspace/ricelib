package space.ayin.rice.model;

import com.ubs_soft.commons.collections.BitBuffer;
import com.ubs_soft.commons.collections.GrowingArrayBitBuffer;
import space.ayin.rice.typemodel.RiceStruct;

import java.util.Arrays;

/**
 * Created by nesferatos on 22.09.2017.
 */
public class RiceValue extends RiceStruct {

    private static transient RiceValue NULL = new RiceValue();

    private int bitLen = 0;
    private byte[] data = new byte[0];

    public int getBitLen() {
        return bitLen;
    }

    public void setBitLen(int bitLen) {
        this.bitLen = bitLen;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public void appendBit(byte bitToAppend) {
        appendBits(1, bitToAppend);
    }

    public void appendBits(byte[] dataToAppend, int bitCountToAppend) {
        appendBits(bitCountToAppend, dataToAppend);
    }

    public void appendBits(int bitCountToAppend, byte... dataToAppend){
        BitBuffer growingArrayBitBuffer = new GrowingArrayBitBuffer();
        if (bitLen > 0) {
            growingArrayBitBuffer.append(data, bitLen);
        }
        growingArrayBitBuffer.append(dataToAppend, bitCountToAppend);
        bitLen = growingArrayBitBuffer.getBitLength();
        data = growingArrayBitBuffer.getData();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RiceValue riceValue = (RiceValue) o;

        if (bitLen != riceValue.bitLen) return false;
        return Arrays.equals(data, riceValue.data);
    }

    @Override
    public int hashCode() {
        int result = (int) (bitLen ^ (bitLen >>> 32));
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    @Override
    public String toString() {
        if (this == NULL) {
            return "◊";
        }
        if (bitLen > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < data.length; i++) {
                byte sFormat = (byte) (((bitLen - (8 * i)) >= 8) ? 8 : (bitLen - (8 * i)));
                String str = String.format("%" + String.valueOf(sFormat) + "s", Integer.toBinaryString(data[i] & 0xFF)).replace(' ', '0');
//                String str = String.valueOf(data[i] & 0xFF);
                stringBuilder.append(str + " ");
            }
            return stringBuilder.toString();
        }
        return "0";
    }

    public static RiceValue getNULL() {
        return NULL;
    }
}
