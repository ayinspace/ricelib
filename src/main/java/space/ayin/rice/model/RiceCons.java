package space.ayin.rice.model;

import space.ayin.rice.typemodel.RiceInternalStruct;

/**
 * Created by nesferatos on 05.10.2017.
 */
public class RiceCons extends RiceInternalStruct {

    private transient static RiceCons NULL_CONS = new RiceCons(true);

    static {
        NULL_CONS.setFirstValue(RiceValue.getNULL());
        NULL_CONS.setSecondValue(RiceValue.getNULL());
        NULL_CONS.setFirstCons(NULL_CONS);
        NULL_CONS.setSecondCons(NULL_CONS);
    }


    private RiceCons firstCons;
    private RiceValue firstValue;

    private RiceCons secondCons;
    private RiceValue secondValue;


    private RiceCons(boolean f) {}

    public RiceCons() {
        setFirstValue(RiceValue.getNULL());
        setSecondValue(RiceValue.getNULL());

        setFirstCons(NULL_CONS);
        setSecondCons(NULL_CONS);
    }

    public RiceCons(RiceValue firstValue, RiceValue secondValue) {
        setFirstValue(firstValue);
        setSecondValue(secondValue);

        setFirstCons(NULL_CONS);
        setSecondCons(NULL_CONS);
    }

    public RiceCons(RiceValue firstValue, RiceCons secondCons) {
        setFirstValue(firstValue);
        setSecondCons(secondCons);

        setFirstCons(NULL_CONS);
        setSecondValue(RiceValue.getNULL());
    }

    public RiceCons(RiceCons firstCons, RiceValue secondValue) {
        setFirstCons(firstCons);
        setSecondValue(secondValue);

        setFirstValue(RiceValue.getNULL());
        setSecondCons(NULL_CONS);
    }

    public RiceCons(RiceCons firstCons, RiceCons secondCons) {
        setFirstCons(firstCons);
        setSecondCons(secondCons);

        setFirstValue(RiceValue.getNULL());
        setSecondValue(RiceValue.getNULL());
    }

    public RiceCons getFirstCons() {
        return firstCons;
    }

    public RiceValue getFirstValue() {
        return firstValue;
    }

    public RiceCons getSecondCons() {
        return secondCons;
    }

    public RiceValue getSecondValue() {
        return secondValue;
    }

    public void setFirstCons(RiceCons firstCons) {
        if (firstCons == null) {
            throw new RuntimeException("can't set null");
        }
        this.firstCons = firstCons;
    }

    public void setFirstValue(RiceValue firstValue) {
        if (firstValue == null) {
            throw new RuntimeException("can't set null");
        }
        this.firstValue = firstValue;
    }

    public void setSecondCons(RiceCons secondCons) {
        if (secondCons == null) {
            throw new RuntimeException("can't set null");
        }
        this.secondCons = secondCons;
    }

    public void setSecondValue(RiceValue secondValue) {
        if (secondValue == null) {
            throw new RuntimeException("can't set null");
        }
        this.secondValue = secondValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RiceCons riceCons = (RiceCons) o;

        if (firstCons != null ? !firstCons.equals(riceCons.firstCons) : riceCons.firstCons != null) return false;
        if (firstValue != null ? !firstValue.equals(riceCons.firstValue) : riceCons.firstValue != null) return false;
        if (secondCons != null ? !secondCons.equals(riceCons.secondCons) : riceCons.secondCons != null) return false;
        return secondValue != null ? secondValue.equals(riceCons.secondValue) : riceCons.secondValue == null;
    }

    @Override
    public int hashCode() {
        int result = firstCons != null ? firstCons.hashCode() : 0;
        result = 31 * result + (firstValue != null ? firstValue.hashCode() : 0);
        result = 31 * result + (secondCons != null ? secondCons.hashCode() : 0);
        result = 31 * result + (secondValue != null ? secondValue.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        if (this == NULL_CONS) {
            return "◊";
        }
        if (firstValue != RiceValue.getNULL() && firstCons != NULL_CONS) {
            throw new RuntimeException("wrong cons in first");
        }
        if (secondValue != RiceValue.getNULL() && secondCons != NULL_CONS) {
            throw new RuntimeException("wrong cons is second");
        }
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("(");
        if (firstValue != RiceValue.getNULL()) {
            stringBuilder.append(firstValue);
        } else if (firstCons != NULL_CONS) {
            stringBuilder.append(firstCons);
        } else {
            stringBuilder.append("◊");
        }
        stringBuilder.append(" . ");
        if (secondValue != RiceValue.getNULL()) {
            stringBuilder.append(secondValue);
        } else if (secondCons != NULL_CONS) {
            stringBuilder.append(secondCons);
        } else {
            stringBuilder.append("◊");
        }
        stringBuilder.append(")");

        return stringBuilder.toString();
    }

    /*public void appendCons(RiceCons newCons) {
        if (firstValue == RiceVM.NULL && firstCons == RiceVM.NULL_CONS) {
            firstCons = newCons;
            return;
        }
        if (secondValue == RiceVM.NULL && secondCons == RiceVM.NULL_CONS) {
            secondCons = newCons;
            return;
        }
        throw new RuntimeException("can't append cons in this case");
    }

    public void appendValue(RiceValue newValue) {
        if (firstValue == RiceVM.NULL && firstCons == RiceVM.NULL_CONS) {
            firstValue = newValue;
            return;
        }
        if (secondValue == RiceVM.NULL && secondCons == RiceVM.NULL_CONS) {
            secondValue = newValue;
            return;
        }
        throw new RuntimeException("can't append value in this case");
    }*/

    public static RiceCons getNullCons() {
        return NULL_CONS;
    }

}
