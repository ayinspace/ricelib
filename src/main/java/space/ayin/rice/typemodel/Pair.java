package space.ayin.rice.typemodel;

public class Pair<LEFT, RIGHT> {

    private LEFT left;

    private RIGHT right;

    public Pair(LEFT left, RIGHT right) {
        this.left = left;
        this.right = right;
    }

    public LEFT getLeft() {
        return left;
    }

    public RIGHT getRight() {
        return right;
    }
}
