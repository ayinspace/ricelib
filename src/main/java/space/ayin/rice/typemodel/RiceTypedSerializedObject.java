package space.ayin.rice.typemodel;

import space.ayin.rice.model.RiceValue;

public class RiceTypedSerializedObject extends RiceStruct {

    private int riceTypeId;

    private RiceValue value;

    public RiceTypedSerializedObject(RiceValue value, int riceTypeId) {
        this.riceTypeId = riceTypeId;
        this.value = value;
    }
}
