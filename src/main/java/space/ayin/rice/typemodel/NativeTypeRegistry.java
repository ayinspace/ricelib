package space.ayin.rice.typemodel;

public interface NativeTypeRegistry {
    Class getTypeById(int typeId);
    Class getTypeByRiceType(RiceType riceType);
    String getFullQualifiedName(RiceType riceType);
    RiceType getRiceType(Class clazz);
}
