package space.ayin.rice.typemodel;

/**
 * Created by nesferatos on 04.08.2017.
 */
public class ArgumentMappingException extends Exception {
    public ArgumentMappingException(String s) {
        super(s);
    }

    public ArgumentMappingException(String s, Throwable cause) {
        super(s, cause);
    }
}
