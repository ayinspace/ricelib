package space.ayin.rice.typemodel;

import java.util.Arrays;

/**
 * Created by nesferatos on 27.09.2017.
 */
public class RiceType extends RiceInternalStruct implements Comparable<RiceType> {

    private RiceType superType;

    private RiceTypeElement[] elements;

    private RiceAnnotation typeAnnotations[];

    private RiceMetaType metaType;

    private String name;

    public RiceType() {
    }

    RiceType(String name, RiceMetaType metaType, RiceType superType) {
        this.name = name;
        this.metaType = metaType;
        this.superType = superType;
    }

    public RiceAnnotation[] getTypeAnnotations() {
        return typeAnnotations;
    }

    public void setTypeAnnotations(RiceAnnotation[] typeAnnotations) {
        this.typeAnnotations = typeAnnotations;
    }

    public RiceMetaType getMetaType() {
        return metaType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMetaType(RiceMetaType metaType) {
        this.metaType = metaType;
    }

    public void setSuperType(RiceType superType) {
        this.superType = superType;
    }

    public RiceType getSuperType() {
        return superType;
    }

    public RiceTypeElement[] getElements() {
        return elements;
    }

    public void setElements(RiceTypeElement[] elements) {
        this.elements = elements;
    }

    public boolean isInstanceOf(RiceType riceType) {
        if (riceType == this) {
            return true;
        }
        RiceType t = getSuperType();
        while (t != null) {
            if (t == riceType) {
                return true;
            }
            t = t.getSuperType();
        }
        return false;
    }

    @Override
    public String toString() {
        return "RiceType{ " +
                name +
                ", meta=" + metaType +
                " }";
    }


    //TODO: name of 'list' RiceType
    @Override
    public boolean equals(Object o) {
        return RiceTypeMatcher.getRiceTypeMatcher().equals(this, o);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
//        if (name.equals("list"))
//            result = 31 * result + Arrays.hashCode(elements);
        return result;
    }

    private String getLongName() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getName());
        RiceType iterator = this.superType;
        while (iterator != null) {
            stringBuilder.insert(0 , ">");
            stringBuilder.insert(0 , iterator.getName());
            iterator = iterator.superType;
        }
        return stringBuilder.toString();
    }

    @Override
    public int compareTo(RiceType o) {
//        if (getSuperType() != null && o.getSuperType() != null) {
//            int superTypeCmr = getSuperType().compareTo(o.getSuperType());
//            if (superTypeCmr != 0) {
//                return superTypeCmr;
//            }
//        }
//        return getLongName().compareTo(o.getLongName());
        return getName().compareTo(o.getName());
    }
}
