package space.ayin.rice.typemodel;

public enum RiceMetaType {
    PRIMITIVE_, ENUM_, STRUCT_
}
