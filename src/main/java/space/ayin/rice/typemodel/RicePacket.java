package space.ayin.rice.typemodel;

import com.ubs_soft.commons.collections.BitBuffer;
import com.ubs_soft.commons.collections.GrowingArrayBitBuffer;
import space.ayin.rice.model.RiceCons;
import space.ayin.rice.model.RiceValue;

import java.util.*;

public class RicePacket extends RiceInternalStruct {

    private List<RicePointer> pointers = new ArrayList<>();

    private List<RiceReference> heap = new ArrayList<>();

    private Map<Object, Integer> heapObjects = new HashMap<>();

    public static final int ROOT_RICE_TYPES_BIT_COUNT = 5;

    public RicePacket() { heap.add(null); }

    public byte[] toByteArray() {
        shrink();
        int headEntityId = MapperUtils.bytesToInt(pointers.get(0).getRelativeTypeId().getData());
        BitBuffer bitBuffer = new GrowingArrayBitBuffer();
        int addressBitCount = MapperUtils.bitCount(heap.size());//
        if (addressBitCount > 6) {
            throw new RuntimeException("address overflow");
        }
        bitBuffer.append(MapperUtils.intToBytes(addressBitCount), 6);
        bitBuffer.append(MapperUtils.intToBytes(headEntityId), ROOT_RICE_TYPES_BIT_COUNT);

        RiceCons cursor;
        for (int i = 1; i < heap.size(); i++) {
            RiceReference riceReference = heap.get(i);
            cursor = riceReference.getHead();

            while (cursor.getSecondCons() != RiceCons.getNullCons()) {
                bitBuffer.append(cursor.getFirstValue().getData(), cursor.getFirstValue().getBitLen());
                cursor = cursor.getSecondCons();
            }

        }

        return bitBuffer.getData();
    }


    public synchronized Pair<RicePointer, RiceReference> structPointer(Object object, int relativeTypeId, int relativeTypeOptionsQuantity) {

        RiceReference struct;
        int pointerAddress;

        if (!heapObjects.containsKey(object)) {
            pointerAddress = heap.size();
            struct = new RiceReference();

            heap.add(struct);
            heapObjects.put(object, pointerAddress);
        } else {
            pointerAddress = heapObjects.get(object);
            struct = heap.get(pointerAddress);
        }


        RiceValue relativeTypeIdRiceValue = new RiceValue();
        if(relativeTypeOptionsQuantity != 0) {
            relativeTypeIdRiceValue.appendBits(MapperUtils.intToBytes(relativeTypeId), MapperUtils.bitCount(relativeTypeOptionsQuantity));
        }
        RicePointer ricePointer = new RicePointer(pointerAddress, relativeTypeIdRiceValue);

        pointers.add(ricePointer);
        //heapObjects.put(object, pointerAddress);

        return new Pair<>(ricePointer, struct);
    }

    public void shrink() {
        for (RicePointer ricePointer : pointers)
            if (!ricePointer.isNull())
                ricePointer.shrink(MapperUtils.bitCount(heap.size()));
    }

    public RicePointer createNullPointer() {
        RiceValue relativeTypeIdRiceValue = new RiceValue();
        RicePointer nullPointer = new RicePointer(0, relativeTypeIdRiceValue);
        pointers.add(nullPointer);
        return nullPointer;
    }

    public List<RicePointer> getPointers() {
        return pointers;
    }

    public List<RiceReference> getHeap() {
        return heap;
    }
}
