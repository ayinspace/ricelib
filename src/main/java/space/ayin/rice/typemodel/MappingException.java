package space.ayin.rice.typemodel;

/**
 * Created by nesferatos on 07.10.2017.
 */
public class MappingException extends Exception {
    public MappingException(String s) {
        super(s);
    }

    public MappingException(String s, Exception e) {
        super(s, e);
    }
}
