package space.ayin.rice.typemodel;

import space.ayin.rice.model.RiceValue;

import java.util.Arrays;

public class RicePointer {
    private RiceValue address;

    private RiceValue relativeTypeId;

    public RicePointer(int address, RiceValue relativeTypeId) {
        if (address == 0) {
            this.address = new RiceValue();
        } else {
            this.address = new RiceValue();
            this.address.appendBits(MapperUtils.intToBytes(address), 32);

            this.relativeTypeId = relativeTypeId;
        }
    }

    public RiceValue getAddress() {
        return address;
    }

    public RiceValue getRelativeTypeId() {
        return relativeTypeId;
    }

    public void shrink(int bitCount) {
        byte[] data = address.getData();
        address.setData(new byte[0]);
        address.setBitLen(0);
        address.appendBits(data, bitCount);
    }

    private static byte[] nullAddress = new byte[]{0};

    public boolean isNull(){
        return address.getBitLen()==0 || Arrays.equals(nullAddress, address.getData()) ;//&& address.getBitLen() == 1 TODO: check it
    }
}
