package space.ayin.rice.typemodel;

public class ClassNotRegisteredException extends RuntimeException {

    public ClassNotRegisteredException(Class clazz) {
        super(clazz.getName());
    }

    public ClassNotRegisteredException(RiceType riceType) {
        super("riceType " + riceType.getName());
    }

    public ClassNotRegisteredException(String message) {
        super(message);
    }
}
