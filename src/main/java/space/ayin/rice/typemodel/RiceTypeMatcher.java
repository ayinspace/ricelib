package space.ayin.rice.typemodel;

import java.util.Arrays;

public class RiceTypeMatcher {
    private static RiceTypeMatcher instance = new RiceTypeMatcher();
    private boolean deepEquals = false;

    public static RiceTypeMatcher getRiceTypeMatcher(){
        return instance;
    }

    public boolean equals(RiceType obj1, Object o2) {
        if (obj1 == o2) return true;
        if (obj1 == null || obj1.getClass() != o2.getClass()) return false;

        RiceType obj2 = (RiceType) o2;

        return obj1.getName().equals(obj2.getName());
//        if (deepEquals) {
//            if (!Arrays.equals(obj1.getElements(), obj2.getElements())) return false;
//            if (obj1.getSuperType() != null ? !obj1.getSuperType().equals(obj2.getSuperType()) : obj2.getSuperType() != null) return false;
//            if (obj1.getMetaType() != obj2.getMetaType()) return false;
//            return true;
//        }
//        if (obj1.getName().equals("list"))
//            return Arrays.equals(obj1.getElements(), obj2.getElements());
//        return true;
    }

    public boolean isDeepEquals() {
        return deepEquals;
    }

    public void setDeepEquals(boolean deepEquals) {
        this.deepEquals = deepEquals;
    }
}
