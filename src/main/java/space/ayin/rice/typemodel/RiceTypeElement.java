package space.ayin.rice.typemodel;

public class RiceTypeElement extends RiceInternalStruct {

    private RiceAnnotation[] elementAnnotations;

    private String name;

    private RiceType type;

    public RiceAnnotation[] getElementAnnotations() {
        return elementAnnotations;
    }

    public void setElementAnnotations(RiceAnnotation[] elementAnnotations) {
        this.elementAnnotations = elementAnnotations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RiceType getType() {
        return type;
    }

    public void setType(RiceType type) {
        this.type = type;
    }
}
