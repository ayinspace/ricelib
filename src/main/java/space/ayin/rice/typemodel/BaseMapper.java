package space.ayin.rice.typemodel;

import com.ubs_soft.commons.collections.BitBuffer;
import com.ubs_soft.commons.collections.BitIterator;
import com.ubs_soft.commons.collections.GrowingArrayBitBuffer;
import space.ayin.rice.codegen.CodeGenUtils;
import space.ayin.rice.model.RiceValue;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nesferatos on 27.09.2017.
 */
public abstract class BaseMapper {

    private Charset charset = Charset.forName("UTF-8");
    private byte[] strTerminator = "\0".getBytes(charset);

    protected BaseTypeRegistry typeRegistry;

    protected CodeGenUtils codeGenUtils;

    public BaseMapper(BaseTypeRegistry typeRegistry) {
        this.typeRegistry = typeRegistry;
        this.codeGenUtils = new CodeGenUtils();
    }

    public BaseTypeRegistry getTypeRegistry() {
        return typeRegistry;
    }

    public RiceStruct deserialize(byte[] bytes) throws ClassNotRegisteredException, IllegalAccessException, InstantiationException {
        return deserialize(bytes, getTypeRegistry().RICE_STRUCT_TYPE);
    }

    public class DeserializeContext {
        private BitIterator iterator;
        private  BitBuffer bitBuffer;
        private  int addressLength;
        private  RiceStruct[] heap;
        private int heapSize;

        public DeserializeContext(byte[] bytes) {
            this.bitBuffer = new GrowingArrayBitBuffer();
            bitBuffer.append(bytes, bytes.length * 8);
            iterator = bitBuffer.bitIterator();
            this.addressLength = MapperUtils.bytesToInt(iterator.nextBits(6));
            heap = new RiceStruct[(int)Math.pow(2, addressLength)];
            putRiceStruct(0, null);
            heapSize = 1;
        }

        public BitIterator getIterator() {
            return iterator;
        }

        public BitBuffer getBitBuffer() {
            return bitBuffer;
        }

        public int getAddressLength() {
            return addressLength;
        }

        public void putRiceStruct(int heapAddress, RiceStruct riceStruct) {
            heap[heapAddress] = riceStruct;
            if (heapAddress >= heapSize) {
                this.heapSize = heapAddress + 1;
            }
        }

        public RiceStruct getFromHeap(int address) {
            if (address >= heapSize) {
                return null;
                //throw new RuntimeException(address + " out of range of heap, heapSize size=" + heapSize);
            }
            return heap[address];
        }

        public int getHeapSize() {
            return heapSize;
        }
    }

    public RiceStruct deserialize(byte[] bytes, RiceType generalType) throws ClassNotRegisteredException, IllegalAccessException, InstantiationException {
        DeserializeContext deserializeContext = new DeserializeContext(bytes);

        int relativeTypeIndex = MapperUtils.bytesToInt(deserializeContext.getIterator().nextBits(RicePacket.ROOT_RICE_TYPES_BIT_COUNT));
        RiceType riceType = getTypeRegistry().getRiceTypeByRelativeIndex(generalType, relativeTypeIndex);
        deserializeContext.putRiceStruct(1, (RiceStruct) getTypeRegistry().getJavaTypeByRiceType(riceType).newInstance());
        //this.deserialize(iterator, heap);
        this.deserialize(deserializeContext);

        return deserializeContext.getFromHeap(1);
    }

    abstract protected void deserialize(DeserializeContext deserializeContext) throws ClassNotRegisteredException;

    protected void ensureListCapacity(List list, int index) {
        while (list.size() <= index)
            list.add(null);
    }

    protected int getAddress(DeserializeContext deserializeContext) {
        return MapperUtils.bytesToInt(deserializeContext.getIterator().nextBits(deserializeContext.getAddressLength()));
    }


    protected boolean retrieveIsNotNull(DeserializeContext deserializeContext) {
        return retrieveBoolean(deserializeContext);
    }

    protected boolean retrieveIsNull(DeserializeContext deserializeContext) {
        return !retrieveBoolean(deserializeContext);
    }

    protected boolean retrieveBoolean(DeserializeContext deserializeContext) {
        return (deserializeContext.getIterator().nextBit() == 1);
    }

    protected byte retrieveByte(DeserializeContext deserializeContext) {
        return deserializeContext.getIterator().nextBits(8)[0];
    }

    protected short retrieveShort(DeserializeContext deserializeContext) {
        return MapperUtils.bytesToShort(deserializeContext.getIterator().nextBits(16));
    }

    protected int retrieveInteger(DeserializeContext deserializeContext) {
        return MapperUtils.bytesToInt(deserializeContext.getIterator().nextBits(32));
    }

    protected int retrieveInt(DeserializeContext deserializeContext) {
        return MapperUtils.bytesToInt(deserializeContext.getIterator().nextBits(32));
    }

    protected long retrieveLong(DeserializeContext deserializeContext) {
        return MapperUtils.bytesToLong(deserializeContext.getIterator().nextBits(64));
    }

    protected int retrieveEnumConstraint(DeserializeContext deserializeContext, int bitLen) {
        return MapperUtils.bytesToInt(deserializeContext.getIterator().nextBits(bitLen));
    }

    protected int retrieveRelativeTypeId(DeserializeContext deserializeContext, int numberOfOptions) {
        int bitLen = MapperUtils.bitCount(numberOfOptions);
        return MapperUtils.bytesToInt(deserializeContext.getIterator().nextBits(bitLen));
    }

    protected String retrieveString(DeserializeContext deserializeContext) {
        if (retrieveIsNull(deserializeContext))
            return null;
        GrowingArrayBitBuffer bb = new GrowingArrayBitBuffer();
        int termLeng = strTerminator.length * 8;
        byte[] cursor = deserializeContext.getIterator().nextBits(termLeng);
        while (!Arrays.equals(cursor, strTerminator)) {
            bb.append(cursor, termLeng);
            cursor = deserializeContext.getIterator().nextBits(termLeng);
        }
        return new String(bb.getData(), charset);
    }

    public abstract RicePacket mapToPacket(RiceStruct root) throws ClassNotRegisteredException ;

    public abstract RicePacket mapRiceMessageToPacket(RiceMessage riceMessage) throws ClassNotRegisteredException ;

    private RiceValue mapbooleanToRiceValue(boolean value) {
        RiceValue riceValue = new RiceValue();
        riceValue.appendBit((byte) (value ? 1 : 0));
        return riceValue;
    }

    private RiceValue mapbyteToRiceValue(byte value) {
        RiceValue riceValue = new RiceValue();
        riceValue.appendBits(new byte[]{value}, 8);
        return riceValue;
    }

    private RiceValue mapShortToRiceValue(short value) {
        RiceValue riceValue = new RiceValue();
        riceValue.appendBits(MapperUtils.shortToBytes(value), 16);
        return riceValue;
    }

    private RiceValue mapIntToRiceValue(int value) {
        RiceValue riceValue = new RiceValue();
        riceValue.appendBits(MapperUtils.intToBytes(value), 32);
        return riceValue;
    }

    private RiceValue mapLongToRiceValue(long value) {
        RiceValue riceValue = new RiceValue();
        riceValue.appendBits(MapperUtils.longToBytes(value), 64);
        return riceValue;
    }

    private RiceValue mapStringToRiceValue(String value) {
        RiceValue riceValue = new RiceValue();
        riceValue.appendBits(value.getBytes(charset), (value.getBytes().length) * 8);
        return riceValue;
    }

    protected RiceValue mapEnumToRiceValue(int constraintNum, int constraintsQuantity) {
        RiceValue riceValue = new RiceValue();
        byte[] bytes = MapperUtils.intToBytes(constraintNum);
        int bitCount = (32 - Integer.numberOfLeadingZeros(constraintsQuantity));
        riceValue.appendBits(bytes, bitCount);
        return riceValue;
    }




    protected void appendNULL(RiceReference riceReference) {
        appendBoolean(false, riceReference);
    }

    protected void appendNOT_NULL(RiceReference riceReference) {
        appendBoolean(true, riceReference);
    }

    protected void appendBoolean(boolean value, RiceReference riceReference) {
        riceReference.append(mapbooleanToRiceValue(value));
    }

    protected void appendByte(byte value, RiceReference riceReference) {
        riceReference.append(mapbyteToRiceValue(value));
    }

    protected void appendShort(short value, RiceReference riceReference) {
        riceReference.append(mapShortToRiceValue(value));
    }

    protected void appendInteger(int value, RiceReference riceReference) {
        riceReference.append(mapIntToRiceValue(value));
    }

    protected void appendInt(int value, RiceReference riceReference) {
        riceReference.append(mapIntToRiceValue(value));
    }

    protected void appendLong(long value, RiceReference riceReference) {
        riceReference.append(mapLongToRiceValue(value));
    }

    protected void appendString(String value, RiceReference riceReference) {
        if (value == null) {
            appendNULL(riceReference);
        } else {
            appendNOT_NULL(riceReference);
            RiceValue strValue = mapStringToRiceValue(value);
            strValue.appendBits(strTerminator, strTerminator.length * 8);
            riceReference.append(strValue);
        }
    }

    protected void appendRicePointer(RicePointer ricePointer, RiceReference riceReference) {
        if (ricePointer.isNull())
            appendNULL(riceReference);
        else {
            appendNOT_NULL(riceReference);

            riceReference.append(ricePointer.getAddress());

            if (ricePointer.getRelativeTypeId().getBitLen() > 0) {
                riceReference.append(ricePointer.getRelativeTypeId());
            }

        }
    }

    private void appendbits(byte data[], int bitLen, RiceReference riceReference) {
        RiceValue riceValue = new RiceValue();
        riceValue.appendBits(data, bitLen);
        riceReference.append(riceValue);
    }

//
//    protected boolean retrieveIsNotNull(RiceReference riceReference){
//        return retrieveBoolean(riceReference);
//    }
//
//    protected int retrieveInt(RiceReference riceReference) {
//        return mapRiceValueToint(riceReference.retrieve());
//    }
//
//    protected String retrieveString(RiceReference riceReference) {
//        if (retrieveIsNotNull(riceReference))
//            return mapRiceValueToString(riceReference.retrieve());
//        else
//            return null;
//    }
//
//    protected boolean retrieveBoolean(RiceReference riceReference){
//        return mapRiceValueToboolean(riceReference.retrieve());
//    }
//    private int mapRiceValueToint(RiceValue riceValue) {
//        if (riceValue.getBitLen() > 32) {
//            throw new RuntimeException("can't convert to int " + riceValue.getBitLen() + " bits");
//        }
//        return MapperUtils.bytesToInt(riceValue.getData());
//    }
//
//    private String mapRiceValueToString(RiceValue riceValue) {
//        byte[] data = riceValue.getData();
//        byte[] dataWithoutTerminator = new byte[data.length - 1];
//        System.arraycopy(data, 0, dataWithoutTerminator, 0, dataWithoutTerminator.length);
//        return new String(dataWithoutTerminator, charset);
//    }
//
//    private boolean mapRiceValueToboolean(RiceValue riceValue) {
//        if (riceValue.getBitLen() != 1) {
//            throw new RuntimeException("only 1 bit riceValue can be mapped on boolean, current bitLen=" + riceValue.getBitLen());
//        }
//        return riceValue.getData()[0] == 1;
//    }
//
//    protected int mapRiceValueToEnum(RiceValue value) {
//        int heap = MapperUtils.bytesToInt(value.getData());
//        return heap;
//    }
}
