package space.ayin.rice.typemodel;

public class JavaTypeRegistry implements NativeTypeRegistry {

    @Override
    public Class getTypeById(int typeId) {
        return null;
    }

    @Override
    public Class getTypeByRiceType(RiceType riceType) {
        return null;
    }

    @Override
    public String getFullQualifiedName(RiceType riceType) {
        return null;
    }

    @Override
    public RiceType getRiceType(Class clazz) {
        return null;
    }
}
