package space.ayin.rice.typemodel;

import com.google.googlejavaformat.java.FormatterException;
import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.rice.codegen.CodeGen;
import space.ayin.rice.typemodel.methods.RiceCustomMethod;
import space.ayin.rice.typemodel.methods.RiceSystemMethod;
import space.ayin.rice.typemodel.methods.calls.RiceCustomMethodCall;
import space.ayin.rice.typemodel.methods.calls.RiceMethodCall;
import space.ayin.rice.typemodel.methods.calls.RiceSystemMethodCall;
import space.ayin.rice.typemodel.methods.returns.RiceCustomMethodReturn;
import space.ayin.rice.typemodel.methods.returns.RiceMethodReturn;
import space.ayin.rice.typemodel.methods.returns.RiceSystemMethodReturn;

import javax.tools.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.*;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by nesferatos on 29.09.2017.
 */
public class BaseTypeRegistry {

    class BaseTypeRegistryEntity {
        RiceType riceType;
        Class clazz;
    }

    /*class InstructionEntity {
        RiceType instructionRiceType;
        RiceType instructionCallRiceType;
        RiceType instructionReturnRiceType;
        Method instructionMethod;
    }*/

    private static Logger logger = LoggerFactory.getLogger(BaseTypeRegistry.class);

    private Map<String, BaseTypeRegistryEntity> riceTypeNameRegistry = new ConcurrentHashMap<>();

    private Map<String, BaseTypeRegistryEntity> javaTypeNameRegistry = new ConcurrentHashMap<>();

    private Map<RiceType, ArrayList<RiceType>> childTypes = new HashMap<>();

    private CodeGen codeGen;

    /*private Map<Method, InstructionEntity> methodInstructionRegistry = new HashMap<>();
    private Map<RiceType, InstructionEntity> instructionRiceTypeRegistry = new HashMap<>();
    private Map<RiceType, InstructionEntity> instructionRiceTypeCallRegistry = new HashMap<>();
    private Map<RiceType, InstructionEntity> instructionRiceTypeReturnRegistry = new HashMap<>();*/


    public final RiceType BOOLEAN_RICE_TYPE;
    public final RiceType I8_RICE_TYPE;
    public final RiceType I16_RICE_TYPE;
    public final RiceType I32_RICE_TYPE;
    public final RiceType I64_RICE_TYPE;
    public final RiceType FLOAT_RICE_TYPE;
    public final RiceType DOUBLE_RICE_TYPE;
    public final RiceType STRING_RICE_TYPE;
    public final RiceType ENUM_ELEMENT_RICE_TYPE;
    public final RiceType LIST_RICE_TYPE;
    public final RiceType RICE_STRUCT_TYPE;
    public final RiceType RICE_TYPE_RICE_TYPE;
    public final RiceType RICE_TYPE_ELEMENT_RICE_TYPE;
    public final RiceType RICE_ANNOTATION_RICE_TYPE;
    public final RiceType RICE_MESSAGE_TYPE;

    public final RiceType RICE_METHOD_RETURN_RICE_TYPE;

    public final RiceType RICE_SYSTEM_METHOD_RICE_TYPE;
    public final RiceType RICE_SYSTEM_METHOD_RETURN_RICE_TYPE;

    public final RiceType RICE_CUSTOM_METHOD_RICE_TYPE;
    public final RiceType RICE_CUSTOM_METHOD_RETURN_RICE_TYPE;

    public final RiceType RICE_METHOD_CALL_RICE_TYPE;

    public final RiceType RICE_SYSTEM_METHOD_CALL_RICE_TYPE;
    public final RiceType RICE_CUSTOM_METHOD_CALL_RICE_TYPE;


    public BaseTypeRegistry() throws IOException {
        BOOLEAN_RICE_TYPE = new RiceType("BOOLEAN", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(BOOLEAN_RICE_TYPE, boolean.class);

        I8_RICE_TYPE = new RiceType("I8", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(I8_RICE_TYPE, byte.class);

        I16_RICE_TYPE = new RiceType("I16", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(I16_RICE_TYPE, short.class);

        I32_RICE_TYPE = new RiceType("I32", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(I32_RICE_TYPE, int.class);

        I64_RICE_TYPE = new RiceType("I64", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(I64_RICE_TYPE, long.class);

        FLOAT_RICE_TYPE = new RiceType("FLOAT", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(FLOAT_RICE_TYPE, float.class);

        DOUBLE_RICE_TYPE = new RiceType("DOUBLE", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(DOUBLE_RICE_TYPE, double.class);

        STRING_RICE_TYPE = new RiceType("STRING", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(STRING_RICE_TYPE, String.class);

        ENUM_ELEMENT_RICE_TYPE = new RiceType("ENUM_ELEMENT", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(ENUM_ELEMENT_RICE_TYPE, Enum.class);

        LIST_RICE_TYPE = new RiceType("LIST", RiceMetaType.PRIMITIVE_, null);
        registerRiceType(LIST_RICE_TYPE, Array.class);

        RICE_STRUCT_TYPE = getOrRegisterJavaType(RiceStruct.class);
        RICE_TYPE_RICE_TYPE = getOrRegisterJavaType(RiceType.class);
        RICE_TYPE_ELEMENT_RICE_TYPE = getOrRegisterJavaType(RiceTypeElement.class);
        RICE_ANNOTATION_RICE_TYPE = getOrRegisterJavaType(RiceAnnotation.class);
        RICE_MESSAGE_TYPE = getOrRegisterJavaType(RiceMessage.class);
        RICE_METHOD_RETURN_RICE_TYPE = getOrRegisterJavaType(RiceMethodReturn.class);


        RICE_SYSTEM_METHOD_RICE_TYPE = getOrRegisterJavaType(RiceSystemMethod.class);
        RICE_SYSTEM_METHOD_RETURN_RICE_TYPE = getOrRegisterJavaType(RiceSystemMethodReturn.class);

        RICE_CUSTOM_METHOD_RICE_TYPE = getOrRegisterJavaType(RiceCustomMethod.class);
        RICE_CUSTOM_METHOD_RETURN_RICE_TYPE = getOrRegisterJavaType(RiceCustomMethodReturn.class);

        RICE_METHOD_CALL_RICE_TYPE = getOrRegisterJavaType(RiceMethodCall.class);

        RICE_SYSTEM_METHOD_CALL_RICE_TYPE = getOrRegisterJavaType(RiceSystemMethodCall.class);
        RICE_CUSTOM_METHOD_CALL_RICE_TYPE = getOrRegisterJavaType(RiceCustomMethodCall.class);

        codeGen = new CodeGen();
    }

    public int getRegisteredRiceTypeQuantity() {
        return childTypes.size();
    }

    private synchronized void registerRiceType(RiceType riceType, Class clazz) {
        BaseTypeRegistryEntity newEntity = new BaseTypeRegistryEntity();
        newEntity.clazz = clazz;
        newEntity.riceType = riceType;
        riceTypeNameRegistry.put(riceType.getName(), newEntity);
        javaTypeNameRegistry.put(clazz.getSimpleName(), newEntity);
        childTypes.put(riceType, null);
//        addChild(riceType.getSuperType(), riceType);
//        childTypes.put(riceType, new ArrayList<>());
    }

    public synchronized void unregisterRiceType(RiceType riceType) {
        BaseTypeRegistryEntity newEntity =  riceTypeNameRegistry.get(riceType.getName());
        riceTypeNameRegistry.remove(newEntity.riceType.getName());
        javaTypeNameRegistry.remove(newEntity.clazz.getSimpleName());
        removeChildFromParents(null, riceType);
    }

    public synchronized void unregisterClassForRiceType(RiceType riceType) {
        riceTypeNameRegistry.get(riceType.getName()).clazz = null;
    }

    public synchronized void unregisterClass(Class clazz) {
        javaTypeNameRegistry.get(clazz.getSimpleName()).clazz = null;
    }

    /**
     * @param parentType should be null for initial invocation
     */
    private synchronized void removeChildFromParents(RiceType parentType, RiceType childType) {
        if (parentType == null) {
            if (!childTypes.containsKey(childType))
                return;
            childTypes.remove(childType);
            parentType = childType.getSuperType();
        } else {
            ArrayList<RiceType> children = childTypes.get(parentType);
            children.remove(childType);
            children.sort(RiceType::compareTo);
            parentType = parentType.getSuperType();
        }
        if (parentType != null)
            removeChildFromParents(parentType, childType);
    }

    public int getChildrenQuantity(RiceType riceType) {
        int result = 0;
        if (!childTypes.containsKey(riceType)) {
            logger.debug("getChildrenQuantity() throwing exception");
            throw new IllegalArgumentException("getChildrenQuantity invoked with nonexistent riceType");
        }
        ArrayList<RiceType> children = childTypes.get(riceType);
        if (children == null) {
            //logger.debug("getChildrenQuantity() returning 0");
            return result;
        }
        //logger.debug("getChildrenQuantity() returning {}", children.size());
        return children.size();
    }

    public int getRelativeRiceTypeIndex(RiceType fieldType, RiceType instanceType) {
        if (!childTypes.containsKey(fieldType) || !childTypes.containsKey(instanceType))
            throw new IllegalArgumentException("getRelativeRiceTypeIndex invoked with nonexistent fieldType - " + fieldType + ", or instanceType - " + instanceType);
        ArrayList<RiceType> children = childTypes.get(fieldType);
        if (children == null) {
            return 0;
            //throw new IllegalArgumentException("getRelativeRiceTypeIndex invoked with fieldType " + fieldType +  " currently having no children");
        }
        return children.indexOf(instanceType)+1;
    }

    public RiceType getRiceTypeByRelativeIndex(RiceType fieldType, int index) {
        if (!childTypes.containsKey(fieldType))
            throw new IllegalArgumentException("getRiceTypeByRelativeIndex invoked with nonexistent fieldType " + fieldType);
        if (index == 0)
            return fieldType;
        ArrayList<RiceType> children = childTypes.get(fieldType);
        if (children.size() < index)
            throw new IllegalArgumentException("getRiceTypeByRelativeIndex for fieldType " + fieldType + " invoked with wrong index=" + index);
        return children.get(index - 1);
    }

    public List<RiceType> getChildren(RiceType fieldType) {
        //logger.debug("getChildren() for RiceType: {}  currentChildren are: {}", fieldType, childTypes.get(fieldType));
        return childTypes.get(fieldType);
    }

    private synchronized void addChild(RiceType parentType, RiceType childType) {
        ArrayList<RiceType> children = childTypes.get(parentType);

        if (children == null) {
            children = new ArrayList<>();
            childTypes.put(parentType, children);
        }

        if(!children.contains(childType)) {
            children.add(childType);
            children.sort(RiceType::compareTo);
            if (parentType.getSuperType() != null)
                addChild(parentType.getSuperType(), childType);
        }
    }

    public Class getJavaTypeByRiceType(RiceType riceType) throws ClassNotRegisteredException {
        if (riceType == LIST_RICE_TYPE) {
            return Array.class;
        }
        BaseTypeRegistryEntity entity = riceTypeNameRegistry.get(riceType.getName());
        if (entity == null || entity.clazz == null) {
            throw new ClassNotRegisteredException(riceType);
        }
        return entity.clazz;
    }

    public boolean isJavaClassRegistered(String className) {
        BaseTypeRegistryEntity entity = javaTypeNameRegistry.get(className);
        if (entity != null) {
            return true;
        } else {
            return false;
        }
    }

    public RiceType getRiceTypeByJavaType(Class clazz) throws ClassNotRegisteredException {
        BaseTypeRegistryEntity entity = javaTypeNameRegistry.get(clazz.getSimpleName());
        if (entity == null) {
            throw new ClassNotRegisteredException(clazz);
        }
        return entity.riceType;
    }

    public Class getJavaTypeByRiceTypeName(String riceTypeName) throws ClassNotRegisteredException {
        BaseTypeRegistryEntity entity = riceTypeNameRegistry.get(riceTypeName);
        if (entity == null) {
            throw new ClassNotRegisteredException("class with riceTypeName: "+riceTypeName);
        }
        return entity.clazz;
    }

    public RiceType getRiceTypeByRiceTypeName(String riceTypeName) throws ClassNotRegisteredException {
        BaseTypeRegistryEntity entity = riceTypeNameRegistry.get(riceTypeName);
        if (entity == null) {
            throw new ClassNotRegisteredException("class with riceTypeName: "+riceTypeName);
        }
        return entity.riceType;
    }

    public void registerRiceAndJavaType(RiceType riceType, Class clazz) {
        throw new RuntimeException("not implemented");
    }

    public void registerRiceType(RiceType riceType) {
        BaseTypeRegistryEntity newEntity = new BaseTypeRegistryEntity();
        newEntity.riceType = riceType;
        riceTypeNameRegistry.put(riceType.getName(), newEntity);
        childTypes.put(riceType, null);
        addChild(riceType.getSuperType(), riceType);
//        childTypes.put(riceType, new ArrayList<>());
    }


    public RiceType getOrRegisterJavaType(Class clazz) {
        if (isJavaBooleanType(clazz))
            return BOOLEAN_RICE_TYPE;
        if (isJavaByteType(clazz))
            return I8_RICE_TYPE;
        if (isJavaShortType(clazz))
            return I16_RICE_TYPE;
        if (isJavaIntType(clazz))
            return I32_RICE_TYPE;
        if (isJavaLongType(clazz))
            return I64_RICE_TYPE;
        if (isJavaFloatType(clazz))
            return FLOAT_RICE_TYPE;
        if (isJavaDoubleType(clazz))
            return DOUBLE_RICE_TYPE;
        if (clazz.equals(String.class))
            return STRING_RICE_TYPE;
        BaseTypeRegistryEntity entity = javaTypeNameRegistry.get(clazz.getSimpleName());
        if (entity != null)
            return entity.riceType;

        RiceType riceType;

        if (clazz.isEnum()) {
            RiceType enumRiceType = createEnumRiceType(clazz);
            registerRiceType(enumRiceType, clazz);
            return enumRiceType;
        } else if (clazz.isArray()) {
            return createArrayRiceType(clazz);
        } else {
            if (!RiceStruct.class.isAssignableFrom(clazz)) {
                throw new RuntimeException("cannot register " + clazz.getName() + ", struct class must extend RiceStruct class");
            }
            riceType = new RiceType(clazz.getSimpleName(), RiceMetaType.STRUCT_, null);

            registerRiceType(riceType, clazz);
            Class parent = clazz.getSuperclass();
            if (parent != null && parent != Object.class) {
                logger.debug("registering superclass " + parent.getName() + " of " + clazz.getName());
                RiceType parentRiceType = getOrRegisterJavaType(parent);
                riceType.setSuperType(parentRiceType);
                // recursive add riceType and sort all children of parentRiceType. The same to all of parentRiceType parents
                addChild(parentRiceType, riceType);
            }
//            childTypes.put(riceType, new ArrayList<>());

            try {
                List<RiceTypeElement> elements = new ArrayList<>();
                Field[] fields = clazz.getDeclaredFields();

                for (Field field : fields) {

                    if (!Modifier.isTransient(field.getModifiers())) {

                        Class<?> type = field.getType();
                        RiceTypeElement elem = new RiceTypeElement();
                        if (type.isArray() && type.getComponentType().equals(RiceType.class)) {
                            elem.setType(createArrayRiceType(type));
                        } else {
                            elem.setType(getOrRegisterJavaType(field.getType()));
                        }

                        elem.setName(field.getName());

                        elements.add(elem);
                    }
                }

                /*List<Method> methods = ReflectionUtils.getMethodsAnnotatedWith(clazz, RiceInstructionMethod.class);


                RiceType genericMethodRiceType;
                RiceType genericMethodCallRiceType;
                RiceType returnRiceType;

                if (!methods.isEmpty()) {
                    boolean isSystemService = (riceType.getName().equals("SystemService"));

                    genericMethodRiceType = new RiceType(riceType.getName() + "_Method", RiceMetaType.STRUCT_, null);
                    if (isSystemService) {
                        genericMethodRiceType.setSuperType(RICE_SYSTEM_METHOD_RICE_TYPE);
                    } else {
                        genericMethodRiceType.setSuperType(RICE_CUSTOM_METHOD_RICE_TYPE);
                    }

                    genericMethodRiceType.elementNames = new String[0];
                    genericMethodRiceType.elementTypes = new RiceType[0];
                    registerRiceType(genericMethodRiceType);

                    genericMethodCallRiceType = new RiceType(riceType.getName() + "_MethodCall", RiceMetaType.STRUCT_, null);
                    if (isSystemService) {
                        genericMethodCallRiceType.setSuperType(RICE_SYSTEM_METHOD_CALL_RICE_TYPE);
                    } else {
                        genericMethodCallRiceType.setSuperType(RICE_CUSTOM_METHOD_CALL_RICE_TYPE);
                    }
                    genericMethodCallRiceType.elementNames = new String[2];
                    genericMethodCallRiceType.elementTypes = new RiceType[2];
                    genericMethodCallRiceType.elementNames[0] = "instance";
                    genericMethodCallRiceType.elementNames[1] = "method";
                    genericMethodCallRiceType.elementTypes[0] = riceType;
                    genericMethodCallRiceType.elementTypes[1] = genericMethodRiceType;
                    registerRiceType(genericMethodCallRiceType);

                    returnRiceType = new RiceType(riceType.getName() + "_MethodReturn", RiceMetaType.STRUCT_, null);
                    if (isSystemService) {
                        returnRiceType.setSuperType(RICE_SYSTEM_METHOD_RETURN_RICE_TYPE);
                    } else {
                        returnRiceType.setSuperType(RICE_CUSTOM_METHOD_RETURN_RICE_TYPE);
                    }
                    returnRiceType.elementNames = new String[0];
                    returnRiceType.elementTypes = new RiceType[0];
                    registerRiceType(returnRiceType);


                    for (Method method : methods) {

                        RiceType methodRiceType = new RiceType(CodeGen.codeGenUtils.createRiceTypeNameForMethod(method, this), RiceMetaType.STRUCT_, genericMethodRiceType);


                        Parameter[] parameters = method.getParameters();

                        methodRiceType.elementNames = new String[parameters.length];
                        methodRiceType.elementTypes = new RiceType[parameters.length];

                        for (int i = 0; i < parameters.length; i++) {
                            RiceType parameterRiceType = getOrRegisterJavaType(parameters[i].getType());
                            methodRiceType.elementNames[i] = parameters[i].getName();
                            methodRiceType.elementTypes[i] = parameterRiceType;
                        }

                        //RiceType methodRiceType = new RiceType(CodeGen.codeGenUtils.createRiceTypeNameForMethod(method), RiceMetaType.STRUCT_, getRICE_METHOD_RICE_TYPE());
                        registerRiceType(methodRiceType);
                    }
                }*/


                elements.sort(Comparator.comparing(RiceTypeElement::getName));


                riceType.setElements(new RiceTypeElement[elements.size()]);
                elements.toArray(riceType.getElements());

            } catch (Exception e) {
                unregisterRiceType(riceType);
                logger.error(e.getMessage(), e);
            }
            return riceType;
        }
    }

    private RiceType createEnumRiceType(Class clazz) {
        RiceType riceType = new RiceType(clazz.getSimpleName(), RiceMetaType.ENUM_, null);
        List<RiceTypeElement> enumElems = new ArrayList<>();
        for (int i = 0; i < clazz.getEnumConstants().length; i++) {
            RiceTypeElement enumElem = new RiceTypeElement();
            enumElem.setName(((Enum) clazz.getEnumConstants()[i]).name());
            enumElem.setType(ENUM_ELEMENT_RICE_TYPE);
            enumElems.add(enumElem);
        }
        riceType.setElements(new RiceTypeElement[enumElems.size()]);
        enumElems.toArray(riceType.getElements());
        return riceType;
    }

    private RiceType createArrayRiceType(Class clazz) {
        int dimensions = 0;
        while (clazz.isArray()) {
            clazz = clazz.getComponentType();
            dimensions++;
        }
        RiceType componentType = getOrRegisterJavaType(clazz);
        String riceTypeName = componentType.getName();
        RiceTypeElement elem = new RiceTypeElement();
        elem.setType(componentType);


        RiceType riceType = null;
        for (; dimensions > 0; dimensions--) {
            riceTypeName = "l_" + riceTypeName;
            riceType = new RiceType(riceTypeName, RiceMetaType.STRUCT_, LIST_RICE_TYPE);
            riceType.setElements(new RiceTypeElement[]{elem});
            componentType = riceType;
            elem = new RiceTypeElement();
            elem.setType(componentType);
        }
        return riceType;
    }

    /*private RiceType createInstructionRiceType(Method method) {
        RiceType methodRiceType = new RiceType();
        methodRiceType.setMetaType(RiceMetaType.INSTRUCTION_);
        methodRiceType.setName(method.getName());
        methodRiceType.setSuperType(getOrRegisterJavaType(method.getReturnType()));

        List<String> parameterNames = new ArrayList<>();
        List<RiceType> parameterTypes = new ArrayList<>();

        for (int i = 0; i < method.getParameterTypes().length; i++) {
            parameterTypes.add(getOrRegisterJavaType(method.getParameterTypes()[i]));
            parameterNames.add(method.getParameters()[i].getName());
        }

        methodRiceType.setElementNames(new String[parameterNames.size()]);
        methodRiceType.setElementTypes(new RiceType[parameterTypes.size()]);

        parameterTypes.toArray(methodRiceType.getElementTypes());
        parameterNames.toArray(methodRiceType.getElementNames());

        return methodRiceType;
    }

    private RiceType createInstructionCallRiceType(RiceType methodRiceType) {
        RiceType instructionCallRiceType = new RiceType();
        instructionCallRiceType.setMetaType(RiceMetaType.STRUCT_);

        instructionCallRiceType.setName(CodeGen.codeGenUtils.createNameForMethodStruct(methodRiceType));


        instructionCallRiceType.setElementNames(new String[methodRiceType.getElementNames().length - 1]);//first is return
        instructionCallRiceType.setElementTypes(new RiceType[methodRiceType.getElementNames().length - 1]);

        System.arraycopy(methodRiceType.getElementNames(), 1, instructionCallRiceType.getElementNames(), 0, instructionCallRiceType.getElementNames().length);
        System.arraycopy(methodRiceType.getElementTypes(), 1, instructionCallRiceType.getElementTypes(), 0, instructionCallRiceType.getElementTypes().length);

        instructionCallRiceType.setSuperType(RICE_CALL_RICE_TYPE);

        return instructionCallRiceType;
    }

    private RiceType createReturnCallRiceType(RiceType methodRiceType) {
        RiceType instructionCallRiceType = new RiceType();
        instructionCallRiceType.setMetaType(RiceMetaType.STRUCT_);

        instructionCallRiceType.setName(CodeGen.codeGenUtils.createNameForMethodReturnStruct(methodRiceType));


        instructionCallRiceType.setElementNames(new String[1]);
        instructionCallRiceType.setElementTypes(new RiceType[1]);

        instructionCallRiceType.getElementNames()[0] =  methodRiceType.getElementNames()[0];
        instructionCallRiceType.getElementTypes()[0] =  methodRiceType.getElementTypes()[0];

        instructionCallRiceType.setSuperType(RICE_RETURN_RICE_TYPE);

        return instructionCallRiceType;
    }

    private void registerInstruction(RiceType instructionRiceType) {
        RiceType instructionCallRiceType = createInstructionCallRiceType(instructionRiceType);
        registerRiceType(instructionCallRiceType);

        RiceType returnCallRiceType = createReturnCallRiceType(instructionRiceType);
        registerRiceType(returnCallRiceType);


        InstructionEntity instructionEntity = new InstructionEntity();

        instructionEntity.instructionCallRiceType = instructionCallRiceType;
        instructionEntity.instructionReturnRiceType = returnCallRiceType;


        instructionEntity.instructionRiceType = instructionRiceType;
        instructionRiceTypeCallRegistry.put(instructionCallRiceType, instructionEntity);
        instructionRiceTypeReturnRegistry.put(returnCallRiceType, instructionEntity);
        instructionRiceTypeRegistry.put(instructionRiceType, instructionEntity);
    }*/

    public static boolean isJavaBooleanType(Class clazz) {
        return (clazz.equals(boolean.class));
    }

    public static boolean isJavaByteType(Class clazz) {
        return (clazz.equals(byte.class));
    }

    public static boolean isJavaShortType(Class clazz) {
        return (clazz.equals(short.class));
    }

    public static boolean isJavaIntType(Class clazz) {
        return (clazz.equals(int.class));
    }

    public static boolean isJavaLongType(Class clazz) {
        return (clazz.equals(long.class));
    }

    public static boolean isJavaFloatType(Class clazz) {
        return (clazz.equals(float.class));
    }

    public static boolean isJavaDoubleType(Class clazz) {
        return (clazz.equals(double.class));
    }

    public static boolean isJavaPrimitive(Class clazz) {
        return isJavaBooleanType(clazz) ||
                isJavaByteType(clazz) ||
                isJavaShortType(clazz) ||
                isJavaIntType(clazz) ||
                isJavaLongType(clazz) ||
                isJavaFloatType(clazz) ||
                isJavaDoubleType(clazz);
    }

    public RiceType getRICE_MESSAGE_TYPE() {
        return RICE_MESSAGE_TYPE;
    }

    public RiceType getRICE_STRUCT_TYPE() {
        return RICE_STRUCT_TYPE;
    }

    public BaseMapper createAndRegisterNecessaryStructClasses(String srcPath, String classPath, String genCodePackage) throws TemplateException, FormatterException, ClassNotRegisteredException, IOException, ClassNotFoundException, InstantiationException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<BaseTypeRegistryEntity> typeEntities = new ArrayList();
        List<RiceType> riceTypes = new ArrayList<>();
        for (String typeName : riceTypeNameRegistry.keySet()) {
            if (riceTypeNameRegistry.get(typeName).clazz == null) {
                typeEntities.add(riceTypeNameRegistry.get(typeName));
                riceTypes.add(riceTypeNameRegistry.get(typeName).riceType);
            }
        }

        Map<String, String> generatedCode = codeGen.genForType(CodeGen.CodeGenLang.JAVA, riceTypes, genCodePackage, this);


        saveAndCompileClasses(generatedCode, srcPath, classPath);

        logger.debug("struct compilation success");

        URLClassLoader classLoader = new URLClassLoader(new URL[]{new File("./").toURI().toURL()});


        for (BaseTypeRegistryEntity typeEntity : typeEntities) {
            logger.debug("loading class " + typeEntity.riceType.getName());
            Class clazz = classLoader.loadClass(genCodePackage + "." + typeEntity.riceType.getName());
            registerRiceType(typeEntity.riceType, clazz);
            logger.debug("class " + typeEntity.riceType.getName() + " loaded");
        }

        riceTypes.add(RICE_TYPE_RICE_TYPE);
        riceTypes.add(RICE_TYPE_ELEMENT_RICE_TYPE);
        riceTypes.add(RICE_ANNOTATION_RICE_TYPE);
        riceTypes.add(RICE_SYSTEM_METHOD_RICE_TYPE);
        riceTypes.add(RICE_CUSTOM_METHOD_RICE_TYPE);
        riceTypes.add(getRiceTypeByRiceTypeName("RiceMetaType"));

        Map<String, String> mapperSrc = codeGen.genMapperForTypes(CodeGen.CodeGenLang.JAVA, riceTypes, genCodePackage, this);

        saveAndCompileClasses(mapperSrc, srcPath, classPath);

        Class<? extends BaseMapper> mapperClass = (Class<? extends BaseMapper>) classLoader.loadClass(genCodePackage + ".Mapper");

        return mapperClass.getConstructor(BaseTypeRegistry.class).newInstance(this);

    }

    class JavaSourceFromString extends SimpleJavaFileObject {
        final String code;

        JavaSourceFromString(String name, String code) {
            super(URI.create("string:///" + name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
            this.code = code;
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return code;
        }
    }

    private void saveAndCompileClasses(Map<String, String> classesSrc, String srcPath, String classPath) throws IOException {
        List<JavaFileObject> javaFileObjects = new ArrayList<>();
        for (String key : classesSrc.keySet()) {

            File dir = new File(srcPath);
            dir.mkdirs();
            File file = new File(srcPath + key);
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(classesSrc.get(key));
            fileWriter.close();
            javaFileObjects.add(new JavaSourceFromString(key.replace(".java", ""), classesSrc.get(key)));
        }

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);

        String outputDir = classPath;
        File outputDirFile = new File(outputDir);
        outputDirFile.mkdirs();
        fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays.asList(outputDirFile));

        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, diagnostics, Arrays.asList("-parameters"), null, javaFileObjects);


        Set<String> brokenClassSet = new HashSet<>();
        StringBuilder brokenClassList = new StringBuilder();
        if (!task.call()) {
            for (Diagnostic diagnostic : diagnostics.getDiagnostics()) {
                logger.error(diagnostic.getCode());
                logger.error(diagnostic.getKind().toString());
                logger.error("line number: " + diagnostic.getLineNumber());
                logger.error(diagnostic.getSource().toString());
                logger.error(diagnostic.getMessage(null));
                if (!brokenClassSet.contains(diagnostic.getSource().toString())) {
                    brokenClassSet.add(diagnostic.getSource().toString());
                    brokenClassList.append(diagnostic.getSource().toString() + ", ");
                }
            }
            throw new RuntimeException(brokenClassList.toString() + "compilation failed");
        }

    }

}
