package space.ayin.rice.typemodel.methods.annotations;

import space.ayin.rice.typemodel.RiceAnnotation;

public class RiceMethodAnnotation extends RiceAnnotation {

    private String methodName;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
