package space.ayin.rice.typemodel.methods;

import space.ayin.rice.typemodel.RiceInstructionMethod;
import space.ayin.rice.typemodel.RiceStruct;

public class RiceSystemService extends RiceStruct {

    @RiceInstructionMethod
    public int sum(int x, int y) {
        return x + y;
    }
}
