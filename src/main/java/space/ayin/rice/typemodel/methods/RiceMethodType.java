package space.ayin.rice.typemodel.methods;

import space.ayin.rice.typemodel.RiceType;

public class RiceMethodType extends RiceType {

    private String methodName;

    private RiceType returnType;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public RiceType getReturnType() {
        return returnType;
    }

    public void setReturnType(RiceType returnType) {
        this.returnType = returnType;
    }
}
