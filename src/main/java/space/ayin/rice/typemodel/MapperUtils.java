package space.ayin.rice.typemodel;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by nesferatos on 07.10.2017.
 */
public class MapperUtils {

    private static final ByteOrder ENDIAN = ByteOrder.LITTLE_ENDIAN;

    public static long bytesToLong(byte[] b) {
        if (b.length > 8) {
            throw new RuntimeException("can't convert to long " + b.length + " bytes");
        }
        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        byteBuffer.order(ENDIAN);
        byteBuffer.put(b);
        byteBuffer.position(0);
        return byteBuffer.getLong();
    }

    public static int bytesToInt(byte[] b) {
        if (b.length > 4) {
            throw new RuntimeException("can't convert to int " + b.length + " bytes");
        }
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(ENDIAN);
        byteBuffer.put(b);
        byteBuffer.position(0);
        return byteBuffer.getInt();
    }

    public static short bytesToShort(byte[] b) {
        if (b.length > 2) {
            throw new RuntimeException("can't convert to short " + b.length + " bytes");
        }
        ByteBuffer byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.order(ENDIAN);
        byteBuffer.put(b);
        byteBuffer.position(0);
        return byteBuffer.getShort();
    }

    public static byte[] longToBytes(long value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        byteBuffer.order(ENDIAN);
        byteBuffer.putLong(value);
        return byteBuffer.array();
    }

    public static byte[] intToBytes(int value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(ENDIAN);
        byteBuffer.putInt(value);
        return byteBuffer.array();
    }

    public static byte[] shortToBytes(short value) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.order(ENDIAN);
        byteBuffer.putShort(value);
        return byteBuffer.array();
    }

    public static int bitCount(int num) {
        return (32 - Integer.numberOfLeadingZeros(num));
    }

}
