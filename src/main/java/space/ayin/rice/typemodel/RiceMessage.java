package space.ayin.rice.typemodel;

public class RiceMessage extends RiceInternalStruct {
    private short sequence;

    public short getSequence() {
        return sequence;
    }

    public void setSequence(short sequence) {
        this.sequence = sequence;
    }
}
