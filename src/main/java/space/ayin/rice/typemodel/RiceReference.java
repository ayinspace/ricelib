package space.ayin.rice.typemodel;

import space.ayin.rice.model.RiceCons;
import space.ayin.rice.model.RiceValue;

public class RiceReference {
    private RiceCons head;
    private RiceCons tail;

    public RiceReference() {
        head = new RiceCons();
        tail = head;
    }

    public void append(RiceValue newValue){
        RiceCons newCons = new RiceCons();
        tail.setFirstValue(newValue);
        tail.setSecondCons(newCons);
        tail = newCons;
    }

    public RiceValue retrieve(){
        RiceValue result = head.getFirstValue();
        head = head.getSecondCons();
        return result;
    }

    public RiceCons getHead() {
        return head;
    }

    public RiceCons getTail() {
        return tail;
    }

    public boolean isEmpty(){
        return head==tail;
    }
}
