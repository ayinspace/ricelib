package space.ayin.rice.test;

import space.ayin.rice.typemodel.*;
import space.ayin.rice.vm.RiceVM;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;



public class RpcTest {

    public static void main(String[] args) throws Exception {
        RiceVM riceVM = new RiceVM(new BaseTypeRegistry());

        RiceType personRiceType = riceVM.getTypeRegistry().getOrRegisterJavaType(Person.class);

        RiceType employeeRiceType = riceVM.getTypeRegistry().getOrRegisterJavaType(Employee.class);

        RiceType organizationRiceType = riceVM.getTypeRegistry().getOrRegisterJavaType(Organization.class);

        RiceType sampleServiceRiceType = riceVM.getTypeRegistry().getOrRegisterJavaType(SampleService.class);

        riceVM.getTypeRegistry().unregisterClass(Person.class);

        riceVM.getTypeRegistry().unregisterClass(Employee.class);

        riceVM.getTypeRegistry().unregisterClass(SampleService.class);

        riceVM.getTypeRegistry().unregisterClass(Organization.class);

        riceVM.getTypeRegistry().unregisterClass(DayOfWeek.class);



        String genCodePackage = "codegen.space.ayin.rice.lib.test";

        String path = "target/generated-sources/java/rice/" + genCodePackage.replace(".", "/") + "/";

        String outputDir = "target/classes";

        BaseMapper mapper = riceVM.getTypeRegistry().createAndRegisterNecessaryStructClasses(path, outputDir, genCodePackage);
        System.out.println();
       /* Class cl = ClassLoader.getSystemClassLoader().loadClass(genCodePackage+".Call_fetchChildren_Person_BOOLEAN");
        Constructor<RiceMessage> cons = cl.getDeclaredConstructors()[0];
        cons.setAccessible(true);
        RiceMessage o = cons.newInstance();
        o.setSequence((short) 23);
        Method m = cl.getDeclaredMethod ("setArg1", new Class[]{boolean.class});
        m.setAccessible(true);
        m.invoke(o, true);*/

        testEquals(mapper, personRiceType);
        testEquals(mapper, organizationRiceType);
        //testEquals(mapper, sampleServiceRiceType);
        //testEquals(mapper, o);
    }

    public static boolean testEquals(BaseMapper mapper, RiceStruct initial) throws InstantiationException, IllegalAccessException {
        RicePacket ricePacket = mapper.mapToPacket(initial);
        byte[] serialized = ricePacket.toByteArray();
        RiceStruct deserialized;
        if(initial instanceof RiceMessage)
            deserialized = mapper.deserialize(serialized, mapper.getTypeRegistry().RICE_MESSAGE_TYPE);
        else
            deserialized = mapper.deserialize(serialized);
        boolean lastDeepEquals = RiceTypeMatcher.getRiceTypeMatcher().isDeepEquals();

        if (!lastDeepEquals)
            RiceTypeMatcher.getRiceTypeMatcher().setDeepEquals(true);
        boolean equals = initial.equals(deserialized);
        if (!lastDeepEquals)
            RiceTypeMatcher.getRiceTypeMatcher().setDeepEquals(false);

        System.out.println("serializerd: "+initial+" equals=" +equals);
        return equals;
    }
}
