package space.ayin.rice.test;

import space.ayin.rice.typemodel.RiceStruct;

/**
 * Created by nesferatos on 01.09.2017.
 */
public class Organization extends RiceStruct {

    private String name; //4

    private Employee owner; //5

    private Person slave; //6

    private Employee[][] employees; //2

    private String[] titles; //7

    private int[][] data; //1

    private boolean legal = false; //3

    private boolean bankrupt; //0

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getOwner() {
        return owner;
    }

    public void setOwner(Employee owner) {
        this.owner = owner;
    }

    public boolean isLegal() {
        return legal;
    }

    public void setLegal(boolean legal) {
        this.legal = legal;
    }

    public boolean getBankrupt() {
        return bankrupt;
    }

    public void setBankrupt(boolean bankrupt) {
        this.bankrupt = bankrupt;
    }

    public Employee[][] getEmployees() {
        return employees;
    }

    public void setEmployees(Employee[][] employees) {
        this.employees = employees;
    }

    public Person getSlave() {
        return slave;
    }

    public void setSlave(Person slave) {
        this.slave = slave;
    }

    public String[] getTitles() {
        return titles;
    }

    public void setTitles(String[] titles) {
        this.titles = titles;
    }

    public int[][] getData() {
        return data;
    }

    public void setData(int[][] data) {
        this.data = data;
    }

    public boolean isBankrupt() {
        return bankrupt;
    }
}
