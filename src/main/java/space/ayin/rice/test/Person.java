package space.ayin.rice.test;

import space.ayin.rice.typemodel.RiceStruct;

public class Person extends RiceStruct {

    private String name;

    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
