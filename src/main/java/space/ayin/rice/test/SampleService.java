package space.ayin.rice.test;

import space.ayin.rice.typemodel.RiceInstructionMethod;
import space.ayin.rice.typemodel.RiceStruct;
import space.ayin.rice.typemodel.RiceType;

public abstract class SampleService extends RiceStruct {

    @RiceInstructionMethod
    public abstract int sum(int a, int b);

    @RiceInstructionMethod
    public abstract Person[] fetchChildren(Person person, boolean blabla);


}
