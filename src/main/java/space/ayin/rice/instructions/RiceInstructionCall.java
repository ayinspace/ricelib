package space.ayin.rice.instructions;

import space.ayin.rice.model.RiceValue;
import space.ayin.rice.typemodel.RiceStruct;

/**
 * Created by nesferatos on 28.09.2017.
 */
public class RiceInstructionCall extends RiceStruct {

    private int instructionId;

    private RiceValue[] arguments;

    public int getInstructionId() {
        return instructionId;
    }

    public void setInstructionId(int instructionId) {
        this.instructionId = instructionId;
    }

    public RiceValue[] getArguments() {
        return arguments;
    }

    public void setArguments(RiceValue[] arguments) {
        this.arguments = arguments;
    }
}
