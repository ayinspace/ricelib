grammar Rice;

@header {
    package space.ayin.rice.antlr;
}

sexpr
   : item* EOF
   ;

item
   : atom
   | list
   ;

list
   : LPAREN item* RPAREN
   ;

atom
   : STRING
   | SYMBOL
   | NUMBER
   ;


STRING
   : '"' ('\\' . | ~ ('\\' | '"'))* '"'
   ;


WHITESPACE
   : (' ' | '\n' | '\t' | '\r') + -> skip
   ;


NUMBER
   : ('+' | '-')? (DIGIT) + ('.' (DIGIT) +)?
   ;


SYMBOL
   : SYMBOL_START (SYMBOL_START | DIGIT)*
   ;


LPAREN
   : '('
   ;


RPAREN
   : ')'
   ;


fragment SYMBOL_START
   : ('a' .. 'z') | ('A' .. 'Z') | '+' | '-' | '*' | '/' | '.'
   ;


fragment DIGIT
   : ('0' .. '9')
   ;
