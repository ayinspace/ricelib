// Generated from Rice.g4 by ANTLR 4.5.1

    package space.ayin.rice.antlr;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RiceParser}.
 */
public interface RiceListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RiceParser#sexpr}.
	 * @param ctx the parse tree
	 */
	void enterSexpr(RiceParser.SexprContext ctx);
	/**
	 * Exit a parse tree produced by {@link RiceParser#sexpr}.
	 * @param ctx the parse tree
	 */
	void exitSexpr(RiceParser.SexprContext ctx);
	/**
	 * Enter a parse tree produced by {@link RiceParser#item}.
	 * @param ctx the parse tree
	 */
	void enterItem(RiceParser.ItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link RiceParser#item}.
	 * @param ctx the parse tree
	 */
	void exitItem(RiceParser.ItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link RiceParser#list}.
	 * @param ctx the parse tree
	 */
	void enterList(RiceParser.ListContext ctx);
	/**
	 * Exit a parse tree produced by {@link RiceParser#list}.
	 * @param ctx the parse tree
	 */
	void exitList(RiceParser.ListContext ctx);
	/**
	 * Enter a parse tree produced by {@link RiceParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(RiceParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link RiceParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(RiceParser.AtomContext ctx);
}