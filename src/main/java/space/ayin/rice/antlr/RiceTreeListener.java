package space.ayin.rice.antlr;

import space.ayin.rice.typemodel.MappingException;
import space.ayin.rice.model.RiceCons;
import space.ayin.rice.model.RiceValue;
import space.ayin.rice.vm.RiceVM;

import java.util.*;

public class RiceTreeListener extends RiceBaseListener {

    private RiceVM riceVM;

    private Deque<RiceCons> stack = new ArrayDeque<>();

    private RiceCons result;


    public RiceTreeListener(RiceVM riceVM) {
        this.riceVM = riceVM;
    }


    public RiceCons getResult() {
        return result;
    }

    @Override
    public void enterAtom(RiceParser.AtomContext context) {
        RiceCons currentCons = stack.pop();
        try {
            if (currentCons.getFirstValue() == RiceValue.getNULL() && currentCons.getFirstCons() == RiceCons.getNullCons()) {
                RiceCons newCons = appendValue(currentCons, atomContextToOperation(context));
                stack.push(newCons);
            } else {
                RiceCons newCons = appendValue(currentCons, atomContextToValue(context));
                stack.push(newCons);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void enterList(RiceParser.ListContext context) {
        RiceCons newCons = new RiceCons();
        if (!stack.isEmpty()) {
            RiceCons currentCons = stack.pop();
            appendCons(currentCons, newCons);
            stack.push(newCons);
            RiceCons doubleFoldedCons = appendCons(newCons, new RiceCons());
            stack.push(doubleFoldedCons);
        } else {
            result = newCons;
            stack.push(newCons);
        }

    }

    @Override
    public void exitList(RiceParser.ListContext ctx) {
        stack.pop();
    }

    private RiceValue atomContextToOperation(RiceParser.AtomContext atomContext) throws MappingException {
        return atomContextToValue(atomContext);
    }

    private RiceValue atomContextToValue(RiceParser.AtomContext atomContext) throws MappingException {
        byte val = Byte.valueOf(atomContext.getText());
        return null;//riceVM.getMapper().mapToRiceValue(val);
    }

    private static RiceCons appendCons(RiceCons riceCons, RiceCons toAppend) {
        if (riceCons.getFirstValue() == RiceValue.getNULL() && riceCons.getFirstCons() == RiceCons.getNullCons()) {
            riceCons.setFirstCons(toAppend);
            return toAppend;
        }
        if (riceCons.getSecondValue() == RiceValue.getNULL() && riceCons.getSecondCons() == RiceCons.getNullCons()) {
            riceCons.setSecondCons(toAppend);
            return toAppend;
        }
        throw new RuntimeException("can't append cons in such situation");
    }

    private static RiceCons appendValue(RiceCons riceCons, RiceValue toAppend) {
        if (riceCons.getFirstValue() == RiceValue.getNULL() && riceCons.getFirstCons() == RiceCons.getNullCons()) {
            riceCons.setFirstValue(toAppend);
            return riceCons;
        }
        if (riceCons.getSecondValue() == RiceValue.getNULL() && riceCons.getSecondCons() == RiceCons.getNullCons()) {
            RiceCons newRiceCons = new RiceCons();
            newRiceCons.setFirstValue(toAppend);
            riceCons.setSecondCons(newRiceCons);
            return newRiceCons;
        }
        throw new RuntimeException("can't append value in such situation");
    }

}
