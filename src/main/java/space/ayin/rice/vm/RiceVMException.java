package space.ayin.rice.vm;

public class RiceVMException extends RuntimeException {
    public RiceVMException(String message) {
        super(message);
    }

    public RiceVMException(String message, Throwable cause) {
        super(message, cause);
    }
}
