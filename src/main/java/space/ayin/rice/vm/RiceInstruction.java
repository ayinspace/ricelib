package space.ayin.rice.vm;

/**
 * Created by nesferatos on 07.10.2017.
 */
public enum RiceInstruction {
    QUOTE,             //0
    EVAL,              //1
    EQL,               //2
    SERIALIZE,         //3
    DESERIALIZE,       //4
    BYTESUM,           //5
    BYTEDIFF,          //6
    SERVICE_CALL,      //7
    SCRIPT_CALL,       //8
    FRAME_CALL,        //9
}
