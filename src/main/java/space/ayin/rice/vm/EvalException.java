package space.ayin.rice.vm;

/**
 * Created by nesferatos on 08.10.2017.
 */
public class EvalException extends Throwable {
    public EvalException(String message, Exception e) {
        super(message, e);
    }

    public EvalException(String s) {
        super(s);
    }
}
