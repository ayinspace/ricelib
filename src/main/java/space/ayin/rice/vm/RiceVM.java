package space.ayin.rice.vm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.rice.model.RiceCons;
import space.ayin.rice.model.RiceValue;
import space.ayin.rice.typemodel.BaseTypeRegistry;
import space.ayin.rice.typemodel.ClassNotRegisteredException;
import space.ayin.rice.typemodel.RiceType;


/**
 * Created by nesferatos on 06.10.2017.
 */
public class RiceVM {

    private static Logger log = LoggerFactory.getLogger(RiceVM.class);

    private transient BaseTypeRegistry baseTypeRegistry;

    private RiceCons memory = null;

    public RiceVM(BaseTypeRegistry baseTypeRegistry) {
        this.baseTypeRegistry = baseTypeRegistry;
    }

    /*public RiceVM(MapperFactory mapperFactory) {
        this.mapper = mapperFactory.createMapper(this);
        //RiceValue.getNULL() =  new RiceValue();
        //memory = new RiceCons();
    }*/

    private RiceCons addFrame(RiceCons rootFrame) {
        return null;
    }

    public RiceCons eval(RiceCons toEval) throws EvalException {
        RiceCons frame = addFrame(memory);
        RiceCons result = eval(toEval, frame);
        return result;
    }

    private RiceCons eval(RiceCons toEval, RiceCons frame) throws EvalException {
        log.trace("eval");


        try {
            RiceValue instructionValue = evalFirst(toEval, frame);
            RiceInstruction riceInstruction = null;//mapper.mapRiceValue(instructionValue, RiceInstructionMethod.class);

            RiceCons instructionArguments = new RiceCons();

            switch (riceInstruction) {

                case QUOTE: {
                    log.trace("quote");
                    return toEval.getSecondCons();
                }


            }


            if (toEval.getSecondCons() != RiceCons.getNullCons()) {
                instructionArguments = evalSecond(toEval.getSecondCons(), frame);
            } else if (toEval.getSecondValue() != RiceValue.getNULL()) {
                instructionArguments = evalSecond(new RiceCons(toEval.getSecondValue(), RiceValue.getNULL()), frame);
            } else if (toEval.getFirstCons() != RiceCons.getNullCons()) {
                if (toEval.getFirstCons().getSecondCons() != RiceCons.getNullCons()) {
                    instructionArguments = evalSecond(toEval.getFirstCons().getSecondCons(), frame);
                } else {
                    instructionArguments = evalSecond(new RiceCons(toEval.getFirstCons().getSecondValue(), RiceValue.getNULL()), frame);
                }
            }


            switch (riceInstruction) {

                case EVAL: {
                    log.trace("EvalEval");
                    return eval(eval(toEval.getSecondCons().getFirstCons(), frame), frame);
                }

                case EQL: {
                    log.trace("eql");
                    return null;//new RiceCons(mapper.mapToRiceValue(instructionArguments.getFirstValue().equals(instructionArguments.getSecondCons().getFirstValue())), RiceValue.getNULL());
                }

                case SERIALIZE: {
                    //return new RiceCons(serialize(instructionArguments, frame), RiceValue.getNULL());
                    return null;
                }

                case DESERIALIZE: {
                    if (toEval.getSecondCons() != RiceCons.getNullCons()) {
                        return deserialize(eval(toEval.getSecondCons(), frame).getFirstValue(), frame);
                    } else {
                        return deserialize(toEval.getSecondValue(), frame);
                        /*RiceCons toSerialize = new RiceCons(toEval.getSecondValue(), NULL);
                        return new RiceCons(serialize(toSerialize), NULL);*/
                    }
                }
                case BYTESUM: {
                    return null;//new RiceCons(byteSum(instructionArguments, frame), RiceValue.getNULL());
                }
                case BYTEDIFF: {
                    return null;//new RiceCons(byteDiff(instructionArguments, frame), RiceValue.getNULL());
                }
                case SERVICE_CALL: {
                    return new RiceCons(serviceCall(toEval.getSecondCons(), frame), RiceValue.getNULL());
                }
            }
        } catch (Exception e) {
            throw new EvalException(e.getMessage(), e);
        }
        throw new EvalException("can't eval cons " + toEval);
    }

    private RiceValue evalFirst(RiceCons riceCons, RiceCons frame) throws EvalException {
        log.trace("evalFirst");
        if (riceCons.getFirstCons() != RiceCons.getNullCons()) {
            RiceCons val = eval(riceCons.getFirstCons().getFirstCons(), frame);
            if (val.getFirstCons() != RiceCons.getNullCons() || val.getSecondValue() != RiceValue.getNULL() || val.getSecondCons() != RiceCons.getNullCons()) {
                throw new EvalException("cannot evalFirst riceCons " + riceCons);
            }
            return val.getFirstValue();
        }
        return riceCons.getFirstValue();
    }

    private RiceCons evalSecond(RiceCons riceCons, RiceCons frame) throws EvalException {
        log.trace("evalSecond");
        RiceValue first;
        if (riceCons.getFirstCons() != RiceCons.getNullCons()) {
            first = eval(riceCons.getFirstCons(), frame).getFirstValue();
        } else {
            first = riceCons.getFirstValue();
        }

        if (riceCons.getSecondCons() != RiceCons.getNullCons()) {
            return new RiceCons(first, evalSecond(riceCons.getSecondCons(), frame));
        }
        return new RiceCons(first, RiceValue.getNULL());
    }


    /*private RiceValue byteSum(RiceCons riceCons, RiceCons frame) throws EvalException {
        log.trace("byteSum");

        RiceValue firstValue = riceCons.getFirstValue();
        RiceValue secondValue = riceCons.getSecondCons().getFirstValue();

        try {
            byte b1 = mapper.mapRiceValue(firstValue, byte.class);
            byte b2 = mapper.mapRiceValue(secondValue, byte.class);
            byte res = (byte) (b1 + b2);
            return mapper.mapToRiceValue(res);
        } catch (MappingException e) {
            throw new EvalException(e.getMessage(), e);
        }
    }

    private RiceValue byteDiff(RiceCons riceCons, RiceCons frame) throws EvalException {
        log.trace("byteSum");

        RiceValue firstValue = riceCons.getFirstValue();
        RiceValue secondValue = riceCons.getSecondCons().getFirstValue();

        try {
            byte b1 = mapper.mapRiceValue(firstValue, byte.class);
            byte b2 = mapper.mapRiceValue(secondValue, byte.class);
            byte res = (byte) (b1 - b2);
            return mapper.mapToRiceValue(res);
        } catch (MappingException e) {
            throw new EvalException(e.getMessage(), e);
        }
    }*/

    public RiceValue serialize(RiceCons toSerialize, RiceType riceType, RiceCons frame) throws ClassNotRegisteredException {
        log.trace("serialize");
//        int typeId = getTypeRegistry().getIdByRiceType(riceType);
//        Class clazz = getTypeRegistry().getJavaTypeById(typeId);
        //return ///Serializer.serialize(toSerialize, clazz);
        return null;
    }

    public RiceCons deserialize(RiceValue toDeserialize, RiceCons frame) {
        log.trace("deserialize");
        return RiceCons.getNullCons();
    }

    /*public RiceValue valueEquals(RiceValue value1, RiceValue value2, RiceCons frame) throws MappingException {
        log.trace("valueEquals");
        return mapper.mapToRiceValue(value1.equals(value2));
    }*/

    public RiceCons serviceCall(RiceCons riceCons, RiceCons frame) {
        log.trace("serviceCall");
        return RiceCons.getNullCons();
    }


    public BaseTypeRegistry getTypeRegistry() {
        return baseTypeRegistry;
    }
}
