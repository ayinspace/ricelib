package space.ayin.rice.codegen;

import com.google.googlejavaformat.java.Formatter;
import com.google.googlejavaformat.java.FormatterException;
import com.google.googlejavaformat.java.JavaFormatterOptions;
import freemarker.cache.ClassTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModel;
import space.ayin.rice.typemodel.*;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.*;

public class CodeGen {

    private Configuration cfg;

    private static Template javaStructTemplate;
    private static Template javaEnumTemplate;
    private static Template javaMapperTemplate;

    private static Template cppEnumTemplate;
    private static Template cppStructTemplate;

    private static final String JAVA_STRUCT_TEMPLATE = "codegen_templates/java_struct_template.ftl";
    private static final String JAVA_ENUM_TEMPLATE = "codegen_templates/java_enum_template.ftl";
    private static final String JAVA_MAPPER_TEMPLATE = "codegen_templates/java_mapper_template.ftl";


    private static JavaFormatterOptions javaFormatterOptions = JavaFormatterOptions.builder().style(JavaFormatterOptions.Style.GOOGLE).build();
    private static Formatter javaFormatter = new Formatter(javaFormatterOptions);


    private static final String CPP_STRUCT_TEMPLATE = "codegen_templates/cpp_struct_template.ftl";
    private static final String CPP_ENUM_TEMPLATE = "codegen_templates/cpp_enum_template.ftl";

    private static BeansWrapper bw = new BeansWrapper();
    private static TemplateHashModel staticModels = bw.getStaticModels();

    public static CodeGenUtils codeGenUtils = new CodeGenUtils();

    public CodeGen() throws IOException {
        cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLogTemplateExceptions(false);
        cfg.setTemplateLoader(new ClassTemplateLoader());

        javaStructTemplate = cfg.getTemplate(JAVA_STRUCT_TEMPLATE);
        javaEnumTemplate = cfg.getTemplate(JAVA_ENUM_TEMPLATE);
        javaMapperTemplate = cfg.getTemplate(JAVA_MAPPER_TEMPLATE);

        cppStructTemplate = cfg.getTemplate(CPP_STRUCT_TEMPLATE);
        cppEnumTemplate = cfg.getTemplate(CPP_ENUM_TEMPLATE);


        JavaFormatterOptions javaFormatterOptions = JavaFormatterOptions.builder().style(JavaFormatterOptions.Style.GOOGLE).build();
        javaFormatter = new Formatter(javaFormatterOptions);
    }

    public Map<String, String> genMapperForTypes(CodeGenLang codeGenLang, List<RiceType> riceTypes, String nameSpace, BaseTypeRegistry baseTypeRegistry) throws IOException, FormatterException, TemplateException, ClassNotRegisteredException {
        Map<String, String> result = new HashMap();
        if (codeGenLang.equals(CodeGenLang.JAVA)) {
            Map<String, Object> context = new HashMap<>();
            context.put("base_type_registry", baseTypeRegistry);
            context.put("code_gen_utils", codeGenUtils);
            context.put("package", nameSpace);
            context.put("statics", staticModels);
            context.put("list_rice_type", baseTypeRegistry.LIST_RICE_TYPE);

            Set<RiceType> filteredForMapperRiceTypes = new HashSet<>();//TODO: remove it
            for (RiceType riceType : riceTypes) {
                filteredForMapperRiceTypes.add(riceType);
                RiceType superType = riceType.getSuperType();
                while (superType != null && !superType.getName().matches("RiceInternalStruct|RiceStruct")) {
                    filteredForMapperRiceTypes.add(superType);
                    superType = superType.getSuperType();
                }
            }
            context.put("rice_types", filteredForMapperRiceTypes);

            Set<RiceType> filteredRiceTypeListElements = new HashSet<>();
            for (RiceType riceType : filteredForMapperRiceTypes) {

                for (RiceTypeElement element : riceType.getElements()) {
                    RiceTypeElement element_iterator = element;
                    while (element_iterator.getType().isInstanceOf(baseTypeRegistry.LIST_RICE_TYPE)) {
                        filteredRiceTypeListElements.add(element_iterator.getType());
                        element_iterator = element_iterator.getType().getElements()[0];
                    }
                }


            }

            context.put("list_elements", filteredRiceTypeListElements);

            formatAndAddJavaClass("Mapper", javaMapperTemplate, context, result);
        } else {
            throw new RuntimeException(codeGenLang + " generation is not implemented yet");
        }

        return result;
    }

    public Map<String, String> genForType(CodeGenLang codeGenLang, List<RiceType> riceTypes, String nameSpace, BaseTypeRegistry baseTypeRegistry) throws IOException, FormatterException, TemplateException, ClassNotRegisteredException {
        Map<String, String> result = new HashMap();
        if (codeGenLang.equals(CodeGenLang.JAVA)) {
            for (RiceType riceType : riceTypes) {
                //List<Writer> writers = new ArrayList<>();//new StringWriter();
                genJavaClassForType(riceType, nameSpace, baseTypeRegistry, result);
            }

        } else if (codeGenLang.equals(CodeGenLang.CPP)) {
            for (RiceType riceType : riceTypes) {
                Writer writer = new StringWriter();
                Map<String, Object> context = new HashMap<>();
                context.put("base_type_registry", baseTypeRegistry);
                context.put("rice_type", riceType);
                context.put("list_rice_type", baseTypeRegistry.LIST_RICE_TYPE);
                context.put("code_gen_utils", codeGenUtils);
                context.put("package", nameSpace);

                if (riceType.getMetaType() == RiceMetaType.ENUM_) {
                    cppEnumTemplate.process(context, writer);
                } else {
                    cppStructTemplate.process(context, writer);
                }
                String res = writer.toString();
//                String res = writer.toString().replace("\r\n", " ");
//                res = res.replace("\n", " ");


//                res = cppFormatter.formatSource(res);

                result.put(riceType.getName() + ".h", res);

            }
        } else {
            throw new RuntimeException(codeGenLang + " generation is not implemented yet");
        }
        return result;
    }

    private static void genJavaClassForType(RiceType riceType, String nameSpace, BaseTypeRegistry baseTypeRegistry, Map<String, String> result) throws FormatterException, TemplateException, IOException, ClassNotRegisteredException {
        Map<String, Object> context = new HashMap<>();
        context.put("base_type_registry", baseTypeRegistry);
        context.put("rice_type", riceType);
        context.put("list_rice_type", baseTypeRegistry.LIST_RICE_TYPE);
        context.put("code_gen_utils", codeGenUtils);
        context.put("package", nameSpace);


        if (riceType.getMetaType() == RiceMetaType.ENUM_) {
            formatAndAddJavaClass(riceType.getName(), javaEnumTemplate, context, result);
        } else {
            formatAndAddJavaClass(riceType.getName(), javaStructTemplate, context, result);
            RiceType superType = riceType.getSuperType();
            while (superType != null) {
                if (!result.containsKey(superType.getName())) {
                    if (baseTypeRegistry.getJavaTypeByRiceTypeName(superType.getName()) == null) {
                        genJavaClassForType(superType, nameSpace, baseTypeRegistry, result);
                    }
                }
                superType = superType.getSuperType();
            }
        }

    }

    private static void formatAndAddJavaClass(String className, Template template, Map<String, Object> context, Map<String, String> map) throws IOException, TemplateException, FormatterException {
        Writer writer = new StringWriter();
        template.process(context, writer);
        String res = writer.toString();
        res = javaFormatter.formatSource(res);
        map.put(className + ".java", res);
    }

    public enum CodeGenLang {
        JAVA, CPP,
    }

}
