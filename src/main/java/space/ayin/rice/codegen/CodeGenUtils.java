package space.ayin.rice.codegen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import space.ayin.rice.typemodel.BaseTypeRegistry;
import space.ayin.rice.typemodel.ClassNotRegisteredException;
import space.ayin.rice.typemodel.RiceMetaType;
import space.ayin.rice.typemodel.RiceType;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CodeGenUtils {

    private static Logger logger = LoggerFactory.getLogger(CodeGenUtils.class);

    public CodeGenUtils() {
    }

    public RiceType getListComponentType(RiceType riceType, BaseTypeRegistry baseTypeRegistry) {
        if (!riceType.isInstanceOf(baseTypeRegistry.LIST_RICE_TYPE)) {
            logger.warn("getListComponentType call with no List RiceType argument: {}",riceType);
            return riceType;
            //throw new IllegalArgumentException("riceType : " + riceType + " is not a list");
        }
        while (riceType.isInstanceOf(baseTypeRegistry.LIST_RICE_TYPE)) {
            riceType = riceType.getElements()[0].getType();
        }
        return riceType;
    }

    public String getListArrayBrackets(RiceType riceType, BaseTypeRegistry baseTypeRegistry) {
        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
        for (; i < getListArrayNumberOfDimensions(riceType, baseTypeRegistry); i++)
            stringBuilder.append("[]");
        if(i == 0)
            logger.warn("zero brackets added for type: {}. returning - {}", riceType, stringBuilder.toString());
        return stringBuilder.toString();
    }

    public String getListArrayBracketsMinusOne(RiceType riceType, BaseTypeRegistry baseTypeRegistry) {
        StringBuilder stringBuilder = new StringBuilder();
        int i = 1;
        for (; i < getListArrayNumberOfDimensions(riceType, baseTypeRegistry); i++)
            stringBuilder.append("[]");
        if(i == 0)
            logger.warn("zero brackets added for type: {}. returning - {}", riceType, stringBuilder.toString());
        return stringBuilder.toString();
    }

    public String getListArrayPointerSymbols(RiceType riceType, BaseTypeRegistry baseTypeRegistry) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < getListArrayNumberOfDimensions(riceType, baseTypeRegistry); i++)
            stringBuilder.append("*");
        return stringBuilder.toString();
    }

    public int getListArrayNumberOfDimensions(RiceType riceType, BaseTypeRegistry baseTypeRegistry) {
        int result = 0;
        while (riceType.isInstanceOf(baseTypeRegistry.LIST_RICE_TYPE)) {
            result++;
            riceType = riceType.getElements()[0].getType();
        }
        return result;
    }

    public Class<Enum> getEnumClassByRiceType(RiceType riceType, BaseTypeRegistry baseTypeRegistry) throws ClassNotRegisteredException {
        if (riceType.getMetaType() != RiceMetaType.ENUM_)
            throw new RuntimeException("riceType: " + riceType + " is not enum, cant getEnumClassByRiceType()");
        Class<Enum> result = baseTypeRegistry.getJavaTypeByRiceType(riceType);
        return result;
    }

    public int getEnumBitCount(Class<Enum> enumClass) {
        return (32 - Integer.numberOfLeadingZeros(enumClass.getEnumConstants().length));
    }

    public List<RiceType> getSuperTypeList(RiceType riceType) {
        List<RiceType> res = new ArrayList<>();
        res.add(riceType);
        while (riceType.getSuperType() != null && !riceType.getSuperType().getName().matches("RiceInternalStruct|RiceStruct")) {
            res.add(riceType.getSuperType());
            riceType = riceType.getSuperType();
        }
        return res;
    }

    public String getRegisteredFQNOrNameByRiceType(RiceType riceType, BaseTypeRegistry baseTypeRegistry){
        if(riceType.isInstanceOf(baseTypeRegistry.LIST_RICE_TYPE))
            return getListComponentType(riceType, baseTypeRegistry).getName()+getListArrayBrackets(riceType, baseTypeRegistry);
        try {
            if(riceType != null)
                return baseTypeRegistry.getJavaTypeByRiceType(riceType).getName();
            else {
                logger.warn("null in getRegisteredFQNOrNameByRiceType");
                throw new RuntimeException("");
            }
        } catch (ClassNotRegisteredException e){
            logger.trace("method call getRegisteredFQNOrNameByRiceType : java class not registered exception for type: {}", riceType);
            return riceType.getName();
        }
    }

    public String createRiceTypeNameForMethod(Method method, BaseTypeRegistry typeRegistry) {
        StringBuilder name = new StringBuilder();
        name.append(method.getDeclaringClass().getSimpleName() + "_Method_");

        name.append(typeRegistry.getOrRegisterJavaType(method.getReturnType()).getName());
        name.append("_");
        name.append(method.getName());

        for (int i = 0; i < method.getParameterTypes().length; i++) {
            name.append("_");
            name.append(typeRegistry.getOrRegisterJavaType(method.getParameterTypes()[i]).getName());
        }
        return name.toString();
    }

    public String getHeaderNameForRiceType(RiceType riceType, BaseTypeRegistry typeRegistry) {
        String result = null;
        try {
            if (riceType.getMetaType() == RiceMetaType.PRIMITIVE_)
                result = typeRegistry.getJavaTypeByRiceType(riceType).getSimpleName();
            else
                result = riceType.getName();
        } catch (Exception e) {
            logger.error("can't generate name for riceType {}", riceType);
        }
        result = result.substring(0,1).toUpperCase().concat(result.substring(1));
        return result;
    }

    public String getJavaPrimitiveNameForRiceType(RiceType riceType, BaseTypeRegistry typeRegistry) {
        if (riceType == null || typeRegistry == null){
            logger.warn("null pointer");
            return "";
        }

        if (riceType.getMetaType().equals(RiceMetaType.PRIMITIVE_)) {
            if (riceType.getName().equals("BOOLEAN")) {
                return "boolean";
            } else if (riceType.getName().equals("I8")) {
                return "byte";
            } else if (riceType.getName().equals("I16")) {
                return "short";
            } else if (riceType.getName().equals("I32")) {
                return "int";
            } else if (riceType.getName().equals("I64")) {
                return "long";
            } else if (riceType.getName().equals("FLOAT")) {
                return "float";
            } else if (riceType.getName().equals("DOUBLE")) {
                return "double";
            } else if (riceType.getName().equals("STRING")) {
                return "String";
            }
        }

        if (riceType.isInstanceOf(typeRegistry.LIST_RICE_TYPE)) {
            RiceType componentType = getListComponentType(riceType, typeRegistry);
            String result = getJavaPrimitiveNameForRiceType(componentType, typeRegistry);
            String brakets = getListArrayBrackets(riceType, typeRegistry);
            return result + brakets;
        } else {
            try {
                return typeRegistry.getJavaTypeByRiceType(riceType).getName();
            } catch (ClassNotRegisteredException e) {
                logger.trace("method call getJavaPrimitiveNameForRiceType : java class not registered exception for type: {}", riceType);
                return riceType.getName();
            }
        }
    }


    public boolean canBeNull(RiceType riceType, BaseTypeRegistry baseTypeRegistry) {
        if (riceType.getMetaType() == RiceMetaType.PRIMITIVE_) {
            if (riceType == baseTypeRegistry.STRING_RICE_TYPE || riceType == baseTypeRegistry.LIST_RICE_TYPE)
                return true;
            else
                return false;
        }
        return true;
    }

    public boolean hasSupertype(RiceType riceType, BaseTypeRegistry baseTypeRegistry) {
        if(riceType == null)
            throw new RuntimeException("gg");
        if (riceType.getSuperType() != null && !riceType.isInstanceOf(baseTypeRegistry.LIST_RICE_TYPE))
                return true;
            else
                return false;
    }

    public String extendsWithRiceType(RiceType riceType, BaseTypeRegistry typeRegistry) {
        String result = "";
            if (hasSupertype(riceType, typeRegistry))
                result = " extends " + getRegisteredFQNOrNameByRiceType(riceType.getSuperType(), typeRegistry);
        return result;
    }

    public void log(String msg) {
        logger.info(msg);
    }



    /*public String createNameForMethodStruct(RiceType methodRiceType) {
        StringBuilder name = new StringBuilder();
        name.append("Method_");
        name.append(methodRiceType.getName());

        for (int i = 1; i < methodRiceType.getElementTypes().length; i++) {
            name.append("_");
            name.append(methodRiceType.getElementTypes()[i].getName());
        }
        return name.toString();
    }*/

    /*public String createNameForMethodReturnStruct(RiceType methodRiceType) {
        StringBuilder name = new StringBuilder();
        name.append("Return_");
        name.append(methodRiceType.getName());
        for (RiceType parameterType : methodRiceType.getElementTypes()) {

            name.append("_");
            name.append(parameterType.getName());
        }
        return name.toString();
    }*/

}
