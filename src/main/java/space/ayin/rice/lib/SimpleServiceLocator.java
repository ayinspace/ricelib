package space.ayin.rice.lib;

import space.ayin.rice.lib.exceptions.RiceServiceInstanceNotFound;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class SimpleServiceLocator implements ServiceLocator {

    private Set<Object> services = new HashSet<>();

    public void registerServiceInstance(Object serviceInstance) {
        services.add(serviceInstance);
    }


    @Override
    public Object getServiceInstanceFor(Class clazz) throws RiceServiceInstanceNotFound {
        for (Object service : services) {
            if (service.getClass() == clazz) {
                return service;
            }
        }
        throw new RiceServiceInstanceNotFound(clazz);
    }
}
