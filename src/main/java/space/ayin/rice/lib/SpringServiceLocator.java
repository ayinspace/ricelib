package space.ayin.rice.lib;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import space.ayin.rice.lib.exceptions.RiceServiceInstanceNotFound;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class SpringServiceLocator implements ServiceLocator {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public Object getServiceInstanceFor(Class clazz) throws RiceServiceInstanceNotFound {
        return applicationContext.getBean(clazz);
    }
}
