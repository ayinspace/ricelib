package space.ayin.rice.lib;

import space.ayin.rice.lib.exceptions.RiceInstructionIdCollisionException;
import space.ayin.rice.lib.exceptions.RiceInstructionIdNotFound;

import java.lang.reflect.Method;

/**
 * Created by nesferatos on 03.08.2017.
 */
public interface InstructionIdProvider {
    long getIdFor(Method method) throws RiceInstructionIdNotFound, RiceInstructionIdCollisionException;
}
