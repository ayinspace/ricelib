package space.ayin.rice.lib;

/**
 * Created by nesferatos on 11.09.2017.
 */

public class Ref<T> {//TODO: move this class to RiceNodeLib project

    long nodeId;
    long objectPublicId;

    private T snapshot;
    private long snapshotTimestamp;

    public Ref(long objectPublicId) {
        this.objectPublicId = objectPublicId;
    }

    public T getSnapshot() {
        if (snapshot == null) {
            snapshot = request();
        }
        return snapshot;
    }

    public T request() {

        snapshotTimestamp = System.currentTimeMillis();
        return null;
    }

    public long getSnapshotTimestamp() {
        return snapshotTimestamp;
    }

    public boolean isRemoteLocked() {
        return false;
    }

    public T remoteLock() {
        return null;
    }

    public void remoteUnlock() {

    }

    public void remoteWait() {

    }

    public void remoteNotify() {

    }

    public void remoteNotifyAll() {

    }
}
