package space.ayin.rice.lib;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by nesferatos on 03.08.2017.
 */
class MethodInvoker {

    private final Method method;
    private final Object serviceInstance;

    MethodInvoker(Object serviceInstance, Method method) {
        this.serviceInstance = serviceInstance;
        this.method = method;
    }

    Object invoke(Object[] args) throws InvocationTargetException, IllegalAccessException {
        return method.invoke(serviceInstance, args);
    }

    Method getMethod() {
        return method;
    }

}
