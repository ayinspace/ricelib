package space.ayin.rice.lib.exceptions;

import java.lang.reflect.Method;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class RiceInstructionIdCollisionException extends Exception {
    public RiceInstructionIdCollisionException(Method riceMethod, long instructionId, long instructionIdBuf) {
        super(riceMethod.getName() + " have multiple ids: " + instructionId + ", " + instructionIdBuf);
    }

    public RiceInstructionIdCollisionException(String message) {
        super(message);
    }
}
