package space.ayin.rice.lib.exceptions;

/**
 * Created by nesferatos on 04.08.2017.
 */
public class RiceCallInvocationFailed extends Exception {
    public RiceCallInvocationFailed(String msg) {
        super(msg);
    }

    public RiceCallInvocationFailed(String msg, Throwable cause) {
        super(msg, cause);
    }

}
