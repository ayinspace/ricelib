package space.ayin.rice.lib.exceptions;

public class IdProviderCycleException extends Exception {
    public IdProviderCycleException(String s) {
        super(s);
    }
}
