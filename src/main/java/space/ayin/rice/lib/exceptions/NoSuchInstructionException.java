package space.ayin.rice.lib.exceptions;

/**
 * Created by nesferatos on 19.09.2017.
 */
public class NoSuchInstructionException extends Exception {
    public NoSuchInstructionException(String s) {
        super(s);
    }
}
