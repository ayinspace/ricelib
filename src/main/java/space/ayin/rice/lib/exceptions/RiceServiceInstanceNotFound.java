package space.ayin.rice.lib.exceptions;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class RiceServiceInstanceNotFound extends Exception {
    public RiceServiceInstanceNotFound(Class clazz) {
        super("instance of " + clazz + " not found");
    }
}
