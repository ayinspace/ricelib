package space.ayin.rice.lib.exceptions;

/**
 * Created by nesferatos on 13.09.2017.
 */
public class MappingOfInvocationResultException extends Exception {
    public MappingOfInvocationResultException(String msg, Exception cause) {
        super(msg, cause);
    }
}
