package space.ayin.rice.lib;

import space.ayin.rice.lib.exceptions.IdProviderCycleException;
import space.ayin.rice.lib.exceptions.RiceInstructionIdCollisionException;
import space.ayin.rice.lib.exceptions.RiceInstructionIdNotFound;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class CompositeIdProvider implements InstructionIdProvider {

    private final List<InstructionIdProvider> instructionIdProviders = new ArrayList<>();

    public CompositeIdProvider(List<InstructionIdProvider> instructionIdProviders) throws IdProviderCycleException {
        for (InstructionIdProvider idProvider : instructionIdProviders) {
            if (idProvider.equals(this)) {
                throw new IdProviderCycleException("cycle in composite id provider");
            }
        }
        this.instructionIdProviders.addAll(instructionIdProviders);
    }

    @Override
    public long getIdFor(Method riceMethod) throws RiceInstructionIdNotFound, RiceInstructionIdCollisionException {
        boolean idFound = false;
        long instructionId = 0;
        for (InstructionIdProvider instructionIdProvider : instructionIdProviders) {
            long instructionIdBuf = instructionIdProvider.getIdFor(riceMethod);
            if (idFound && (instructionIdBuf != instructionId)) {
                throw new RiceInstructionIdCollisionException(riceMethod, instructionId, instructionIdBuf);
            }
            idFound = true;
            instructionId = instructionIdBuf;
        }
        if (!idFound) {
            throw new RiceInstructionIdNotFound();
        }
        return instructionId;
    }
}
