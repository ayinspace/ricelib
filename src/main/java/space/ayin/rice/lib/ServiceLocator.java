package space.ayin.rice.lib;

import space.ayin.rice.lib.exceptions.RiceServiceInstanceNotFound;

/**
 * Created by nesferatos on 03.08.2017.
 */
public interface ServiceLocator {
    Object getServiceInstanceFor(Class clazz) throws RiceServiceInstanceNotFound;
}
