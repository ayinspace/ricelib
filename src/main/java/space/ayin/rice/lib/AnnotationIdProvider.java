package space.ayin.rice.lib;

import java.lang.reflect.Method;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class AnnotationIdProvider implements InstructionIdProvider {
    @Override
    public long getIdFor(Method method) {
        return method.getAnnotation(InstructionId.class).id();
    }
}
