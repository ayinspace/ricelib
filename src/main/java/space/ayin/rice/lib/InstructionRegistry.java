package space.ayin.rice.lib;

import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import space.ayin.rice.lib.exceptions.RiceInstructionIdCollisionException;
import space.ayin.rice.lib.exceptions.RiceInstructionIdNotFound;
import space.ayin.rice.lib.exceptions.RiceServiceInstanceNotFound;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class InstructionRegistry {

    private final ServiceLocator serviceLocator;
    private final Map<Long, MethodInvoker> invokerRegistry = new HashMap<>();
    private final InstructionIdProvider instructionIdProvider;

    public InstructionRegistry(ServiceLocator serviceLocator, InstructionIdProvider instructionIdProvider) {
        this.serviceLocator = serviceLocator;
        this.instructionIdProvider = instructionIdProvider;
    }

    public void scanPackage(String packageUri) throws RiceInstructionIdNotFound, RiceServiceInstanceNotFound, RiceInstructionIdCollisionException {
        Reflections reflections = new Reflections(packageUri,
                new MethodAnnotationsScanner(),
                new TypeAnnotationsScanner(),
                new SubTypesScanner());
        Set<Class<?>> riceServices = reflections.getTypesAnnotatedWith(Service.class);
        for (Class<?> clazz : riceServices) {
            Set<Method> riceMethods = reflections.getMethodsAnnotatedWith(Instruction.class);
            for (Method riceMethod : riceMethods) {
                if (riceMethod.getDeclaringClass().isAssignableFrom(clazz)) {
                    long instructionId = instructionIdProvider.getIdFor(riceMethod);
                    Object serviceInstance = serviceLocator.getServiceInstanceFor(clazz);
                    invokerRegistry.put(instructionId, new MethodInvoker(serviceInstance,riceMethod));
                }
            }
        }
    }

    Map<Long, MethodInvoker> getInvokerRegistry() {
        return Collections.unmodifiableMap(invokerRegistry);
    }

}
