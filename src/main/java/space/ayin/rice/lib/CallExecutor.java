package space.ayin.rice.lib;

import space.ayin.rice.instructions.RiceInstructionCall;
import space.ayin.rice.lib.exceptions.MappingOfInvocationResultException;
import space.ayin.rice.lib.exceptions.NoSuchInstructionException;
import space.ayin.rice.lib.exceptions.RiceCallInvocationFailed;
import space.ayin.rice.model.RiceValue;
import space.ayin.rice.vm.RiceVM;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by nesferatos on 03.08.2017.
 */
public class CallExecutor {

    private final InstructionRegistry instructionRegistry;

    public CallExecutor(InstructionRegistry instructionRegistry) {
        this.instructionRegistry = instructionRegistry;
    }


    public Object execute(RiceInstructionCall riceCall) throws RiceCallInvocationFailed, NoSuchMethodException, InvocationTargetException, MappingOfInvocationResultException, NoSuchInstructionException {
        long instructionId = riceCall.getInstructionId();
        MethodInvoker methodInvoker = instructionRegistry.getInvokerRegistry().get(riceCall.getInstructionId());

        if (methodInvoker == null) {
            throw new NoSuchInstructionException("no methods with id " + instructionId);
        }

        Object invokeResult;
        try {
            invokeResult = methodInvoker.invoke(riceCall.getArguments());
        } catch (InvocationTargetException | IllegalAccessException e) {
            throw new RiceCallInvocationFailed(methodInvoker.getMethod().getName() +
                    " with id " + instructionId + " invocation failed", e);
        }
        try {
            if (methodInvoker.getMethod().getReturnType().equals(void.class)) {
                return RiceValue.getNULL();
            }
            return invokeResult;
        } catch (Exception e) {
            throw new MappingOfInvocationResultException("failed result mapping", e);
        }
    }

}
