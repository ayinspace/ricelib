package space.ayin.rice.lib.reflection;

/**
 * Created by nesferatos on 10.08.2017.
 */
public class ArrayElemPropertyDescriptor implements PropertyDescriptor {

    private Object[] array;

    private int elemNum;

    public ArrayElemPropertyDescriptor(Object[] array, int elemNum) {
        this.array = array;
        this.elemNum = elemNum;
    }

    @Override
    public Object getValue() {
        return array[elemNum];
    }

    @Override
    public void setValue(Object value) {
        array[elemNum] = value;
    }
}
