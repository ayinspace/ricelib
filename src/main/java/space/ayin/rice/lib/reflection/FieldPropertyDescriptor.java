package space.ayin.rice.lib.reflection;

/**
 * Created by nesferatos on 10.08.2017.
 */
public class FieldPropertyDescriptor implements PropertyDescriptor {

    private String propertyName;

    private Object object;

    public FieldPropertyDescriptor(Object object, String propertyName) {
        this.object = object;
        this.propertyName = propertyName;
    }

    @Override
    public Object getValue() {
        return null;
    }

    @Override
    public void setValue(Object value) {

    }
}
