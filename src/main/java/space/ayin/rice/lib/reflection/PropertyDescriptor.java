package space.ayin.rice.lib.reflection;

/**
 * Created by nesferatos on 08.08.2017.
 */
public interface PropertyDescriptor {
    Object getValue();
    void setValue(Object value);
}
