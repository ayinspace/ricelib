package ${package};

public enum ${rice_type.getName()} {
    <#list rice_type.getElements() as element >
        ${element.getName()}, // ${element_index}
    </#list>
}

