#include <string>

using namespace std;

class ${rice_type.getName()} {

    private:
<#list rice_type.getElementNames() as fieldName >

    /**
    * field # = ${fieldName_index}
    */
<@field name=fieldName type=rice_type.getElementTypes()[fieldName_index]/>;</#list>

    public:
<#list rice_type.getElementNames() as fieldName ><@field_getter name=fieldName type=rice_type.getElementTypes()[fieldName_index]/><@field_setter name=fieldName type=rice_type.getElementTypes()[fieldName_index]/></#list>
};



<#macro field name type><@type_name type=type/> ${name}</#macro>

<#macro field_getter name type> <@type_name type=type/> get${name ? cap_first }() {
    return this->${name};
    }

</#macro>

<#macro field_setter name type>void set${ name ? cap_first }(<@type_name type=type/> _${name}) {
${name} = _${name};
    }

</#macro>

<#macro type_name type><#if type.getMetaType() == ("PRIMITIVE_") ><#if type.getName() == "BOOLEAN" >    bool<#elseif type.getName() == "I8" >  signed char<#elseif type.getName() == "I16" >  short<#elseif type.getName() == "I32" >    long<#elseif type.getName() == "I64" > long long<#elseif type.getName() == "FLOAT" >  float<#elseif type.getName() == "DOUBLE" > double<#elseif type.getName() == "STRING" >    string</#if><#elseif type.isInstanceOf(list_rice_type)> <@type_name type=code_gen_utils.getListType(type, rice_vm)/>${code_gen_utils.getListArrayPointerSymbols(type, rice_vm)}<#else> ${type.getName()}</#if></#macro>