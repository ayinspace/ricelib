package ${package};

import java.util.List;
import space.ayin.rice.model.*;
import space.ayin.rice.typemodel.*;
import space.ayin.rice.typemodel.methods.*;
import org.apache.commons.lang3.builder.EqualsBuilder;

public class ${rice_type.getName()} ${code_gen_utils.extendsWithRiceType(rice_type, base_type_registry)}{

    <#list rice_type.getElements() as typeElem >
        <#--<#if !rice_type.getElementTypes()[fieldName_index].isInstanceOf(base_type_registry.getRICE_METHOD_RICE_TYPE()) >-->
        /**
        * field # = ${typeElem_index}
        */
        <@field name=typeElem.getName() type=typeElem.getType()/>;
        <#--</#if>-->
    </#list>

    <#list rice_type.getElements() as typeElem >
        <#--<#if !rice_type.getElementTypes()[fieldName_index].isInstanceOf(base_type_registry.getRICE_METHOD_RICE_TYPE()) >-->
        <@field_getter name=typeElem.getName() type=typeElem.getType()/>
        <@field_setter name=typeElem.getName() type=typeElem.getType()/>
        <#--</#if>-->
    </#list>

    <#--<#list rice_type.getElementNames() as fieldName >
        <#if rice_type.getElementTypes()[fieldName_index].isInstanceOf(base_type_registry.getRICE_METHOD_RICE_TYPE()) >
            <@method rice_method_type=rice_type.getElementTypes()[fieldName_index]/>
        </#if>
    </#list>-->

    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    <#--<@equals_method rice_type/>-->
}



<#macro field name type>
    private ${code_gen_utils.getJavaPrimitiveNameForRiceType(type, base_type_registry)} ${name}
</#macro>

<#macro field_getter name type>
    public ${code_gen_utils.getJavaPrimitiveNameForRiceType(type, base_type_registry)} get${ name ? cap_first }() {
        return this.${name};
    }
</#macro>

<#macro field_setter name type>
    public void set${ name ? cap_first }(${code_gen_utils.getJavaPrimitiveNameForRiceType(type, base_type_registry)} ${name}) {
        this.${name} = ${name};
    }
</#macro>

<#macro equals_method rice_type>
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass())
            return false;

    ${rice_type.getName()} rhs = (${rice_type.getName()}) obj;
        return new EqualsBuilder()
        .appendSuper(super.equals(obj))
    <#list rice_type.getElementNames() as element>
        .append(${element}, rhs.${element})
    </#list>
        .isEquals();
    }
</#macro>


<#--<#macro method rice_method_type>-->
    <#--public ${code_gen_utils.getJavaPrimitiveNameForRiceType(rice_method_type.getSuperType(), base_type_registry)} ${rice_method_type.getName()}(-->
    <#--<#list rice_method_type.getElementTypes() as arg_type>-->
        <#--<#if arg_type_index gt 0 >-->
        <#--${code_gen_utils.getJavaPrimitiveNameForRiceType(arg_type, base_type_registry)} ${rice_method_type.getElementNames()[arg_type_index]}<#sep>, </#sep>-->
        <#--</#if>-->
    <#--</#list>-->
    <#--) {-->
        <#--&lt;#&ndash;${code_gen_utils.createNameForMethodCallStruct(rice_method_type)} call = new ${code_gen_utils.createNameForMethodCallStruct(rice_method_type)}();&ndash;&gt;-->
        <#--&lt;#&ndash;Object call = new Object();-->
        <#--<#list rice_method_type.getElementTypes() as arg_type>-->
            <#--<#if arg_type_index gt 0 >-->
            <#--call.set${rice_method_type.getElementNames()[arg_type_index]?cap_first}(${rice_method_type.getElementNames()[arg_type_index]});-->
            <#--</#if>-->
        <#--</#list>-->

        <#--<#if rice_method_type.getSuperType().getMetaType() == "PRIMITIVE_">-->
            <#--<#if rice_method_type.getSuperType().getName() == "STRING">-->
                <#--${code_gen_utils.getJavaPrimitiveNameForRiceType(rice_method_type.getSuperType(), base_type_registry)} return_ = null;-->
            <#--<#else>-->
                <#--${code_gen_utils.getJavaPrimitiveNameForRiceType(rice_method_type.getSuperType(), base_type_registry)} return_ = 0;-->
            <#--</#if>-->
        <#--<#else>-->
            <#--${code_gen_utils.getJavaPrimitiveNameForRiceType(rice_method_type.getSuperType(), base_type_registry)} return_ = null;-->
        <#--</#if>-->
        <#--return return_;&ndash;&gt;-->
        <#--return null;-->
    <#--}-->
<#--</#macro>-->
