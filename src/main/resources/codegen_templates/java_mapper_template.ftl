package ${package};

import space.ayin.rice.typemodel.*;
import space.ayin.rice.typemodel.methods.*;
import space.ayin.rice.typemodel.methods.calls.*;
import space.ayin.rice.typemodel.methods.returns.*;
import com.ubs_soft.commons.collections.BitBuffer;
import com.ubs_soft.commons.collections.GrowingArrayBitBuffer;
import space.ayin.rice.model.RiceCons;
import space.ayin.rice.model.RiceValue;
import java.util.ArrayList;
import java.util.List;
import com.ubs_soft.commons.collections.BitIterator;

public class Mapper extends BaseMapper {

    public Mapper(BaseTypeRegistry typeRegistry) {
        super(typeRegistry);
    }

    public RicePacket mapToPacket(RiceStruct riceStruct) throws ClassNotRegisteredException {
        RiceType riceType = getTypeRegistry().getRiceTypeByJavaType(riceStruct.getClass());
        RicePacket ricePacket = new RicePacket();
        RiceType baseEntityRiceType = getTypeRegistry().getRICE_STRUCT_TYPE();
        <#list rice_types>
                switch(riceType.getName()){
            <#items as rice_type>
                <#if rice_type.getMetaType() != "ENUM_"> <#-- FIXME: if rice_types contains only one ricetype - enum - this code will fails -->
                    case "<@type_simple_name rice_type/>": map((<@type_full_name rice_type/>) riceStruct, baseEntityRiceType, ricePacket);
                    break;
                </#if>
            </#items>
                }
        </#list>
        return ricePacket;
    }

    @Override
    public RicePacket mapRiceMessageToPacket(RiceMessage riceMessage) throws ClassNotRegisteredException {
        RiceType riceType = getTypeRegistry().getRiceTypeByJavaType(riceMessage.getClass());
        RicePacket ricePacket = new RicePacket();
        RiceType baseEntityRiceType = getTypeRegistry().getRICE_MESSAGE_TYPE();
        <#list base_type_registry.getChildren(base_type_registry.getRICE_MESSAGE_TYPE())>
        switch(riceMessage.getClass().getSimpleName()){
        <#items as rice_type>
            case "<@type_full_name rice_type/>": map((<@type_full_name rice_type/>) riceMessage, baseEntityRiceType, ricePacket);
            break;
        </#items>
        }
        </#list>
        return ricePacket;
    }

    public byte[] packMessage(RiceMessage riceMessage) throws ClassNotRegisteredException {
            RiceType riceType = getTypeRegistry().getRiceTypeByJavaType(riceMessage.getClass());
            RicePacket ricePacket = new RicePacket();
            RiceType baseEntityRiceType = getTypeRegistry().RICE_MESSAGE_TYPE;
            <#list base_type_registry.getChildren(base_type_registry.getRICE_MESSAGE_TYPE())>
            switch(riceMessage.getClass().getSimpleName()){
                <#items  as child_type>
                 case "<@type_full_name child_type/>": map((<@type_full_name child_type/>) riceMessage, baseEntityRiceType, ricePacket);
                 break;
                </#items>
             }
            </#list>
            return ricePacket.toByteArray();
    }


    public RiceMessage unpackMessage(byte[] data) {
        BitBuffer bitBuffer = new GrowingArrayBitBuffer();
        bitBuffer.append(data, data.length * 8);
        BitIterator bitIterator = bitBuffer.bitIterator();
        // deserializeRiceMessage(bitIterator, ); <#-- FIXME: -->
        return null;
    }

    @Override
    protected void deserialize(DeserializeContext deserializeContext) throws ClassNotRegisteredException {
        int entityIndex = 1;
        while(deserializeContext.getIterator().hasNext() && deserializeContext.getHeapSize() > entityIndex) {
            <#list rice_types>
            switch(deserializeContext.getFromHeap(entityIndex).getClass().getSimpleName()){
                <#items as rice_type>
                    <#if rice_type.getMetaType() == "STRUCT_">
                    case "<@type_simple_name rice_type/>" : entityIndex = deserialize<@type_simple_name rice_type/>(deserializeContext, entityIndex);
                        break;
                    </#if>
                </#items>
                    default: throw new ClassNotRegisteredException("cant map type: " + deserializeContext.getFromHeap(entityIndex).getClass().getSimpleName());
            }
            </#list>
        }
    }

<#list rice_types as rice_type >
    <#if rice_type.getMetaType() == "ENUM_">
        <@append_enum_method rice_type/>
        <@retrieve_byte_enum_method rice_type/>
    <#else >
        <@map_method rice_type/>
        <@append_fields_method rice_type/>
        <@deserialize_byte_method rice_type/>
        <@retrieve_byte_fields_method rice_type/>
        <@retrieve_byte_rice_pointer rice_type/>
    </#if>
</#list>
<#list list_elements as rice_type >
    <@append_list_method rice_type/>
    <@retrieve_byte_list rice_type/>
</#list>

}

<#--<#macro type_full_name rice_type><#if rice_type.getMetaType() == "PRIMITIVE_">${base_type_registry.getJavaTypeByRiceType(rice_type).getSimpleName()?cap_first}<#else>${rice_type.getName()?cap_first}</#if></#macro>-->
<#--<#macro type_simple_name rice_type><#if rice_type.getMetaType() == "PRIMITIVE_">${base_type_registry.getJavaTypeByRiceType(rice_type).getSimpleName()?cap_first}<#else>${rice_type.getName()?cap_first}</#if></#macro>-->
<#--<#macro type_list_name type><#if type.isInstanceOf(list_rice_type)>${type.getName()}<#elseif type.getMetaType() == ("PRIMITIVE_") ><#if type.getName() == "BOOLEAN" >Boolean<#elseif type.getName() == "I8" >Byte<#elseif type.getName() == "I16" >Short<#elseif type.getName() == "I32" >Integer<#elseif type.getName() == "I64" >Long<#elseif type.getName() == "FLOAT" >Float<#elseif type.getName() == "DOUBLE" >Double<#elseif type.getName() == "STRING" >String</#if><#else>${type.getName()}</#if></#macro>-->
<#--<#macro type_name type><#if type.getMetaType() == ("PRIMITIVE_") ><#if type.getName() == "BOOLEAN" >Boolean<#elseif type.getName() == "I8" >Byte<#elseif type.getName() == "I16" >Short<#elseif type.getName() == "I32" >Integer<#elseif type.getName() == "I64" >Long<#elseif type.getName() == "FLOAT" >Float<#elseif type.getName() == "DOUBLE" >Double<#elseif type.getName() == "STRING" >String</#if><#elseif type.isInstanceOf(list_rice_type)><@type_name type=code_gen_utils.getListComponentType(type, base_type_registry)/>${code_gen_utils.getListArrayBrackets(type, base_type_registry)}<#else>${type.getName()}</#if></#macro>-->
<#--<#macro type_primitive_name type><#if type.getMetaType() == ("PRIMITIVE_") ><#if type.getName() == "BOOLEAN" >boolean<#elseif type.getName() == "I8" >byte<#elseif type.getName() == "I16" >short<#elseif type.getName() == "I32" >int<#elseif type.getName() == "I64" >long<#elseif type.getName() == "FLOAT" >float<#elseif type.getName() == "DOUBLE" >double<#elseif type.getName() == "STRING" >String</#if><#elseif type.isInstanceOf(list_rice_type)><@type_primitive_name type=code_gen_utils.getListComponentType(type, base_type_registry)/>${code_gen_utils.getListArrayBrackets(type, base_type_registry)}<#else><@type_simple_name type/></#if></#macro>-->

<#macro type_full_name rice_type>${code_gen_utils.getRegisteredFQNOrNameByRiceType(rice_type, base_type_registry)}</#macro>
<#macro type_simple_name rice_type>${code_gen_utils.getHeaderNameForRiceType(rice_type, base_type_registry)}</#macro>
<#macro type_primitive_name rice_type>${code_gen_utils.getJavaPrimitiveNameForRiceType(rice_type, base_type_registry)}</#macro>

<#macro map_method type>private RicePointer map(<@type_full_name type/> object,RiceType fieldRiceType, RicePacket ricePacket){
    if (object == null) {
        return ricePacket.createNullPointer();
    }
    RiceType objectRiceType = getTypeRegistry().getRiceTypeByJavaType(object.getClass());
    Pair < RicePointer, RiceReference> p = ricePacket.structPointer(object, getTypeRegistry().getRelativeRiceTypeIndex(fieldRiceType, objectRiceType), getTypeRegistry().getChildrenQuantity(fieldRiceType));
    RiceReference reference = p.getRight();
    if(reference.isEmpty()){
        <#list code_gen_utils.getSuperTypeList(type) as superType>
        append<@type_simple_name superType/>Fields(object, objectRiceType, ricePacket, reference);
        </#list>
    }
    return p.getLeft();
}
</#macro>

<#macro append_fields_method type>private void append<@type_simple_name type/>Fields(<@type_full_name type/> object, RiceType objectRiceType, RicePacket ricePacket, RiceReference reference){
    RiceType fieldElementRiceType;
    <#list type.getElements() as element>
        <#if element.getType().getMetaType() != 'INSTRUCTION_'>
            <#if element.getType().isInstanceOf(list_rice_type)>
                appendList(object.get${element.getName()?cap_first}(), ricePacket, reference);
            <#elseif element.getType().getMetaType() == 'ENUM_' || element.getType().getMetaType() == 'PRIMITIVE_'>
                append<@type_simple_name element.getType()/>(object.get${element.getName()?cap_first}(), reference);
            <#elseif element.getType().getMetaType() == 'STRUCT_'>
                fieldElementRiceType = objectRiceType.getElements()[${element?index}].getType();
                appendRicePointer(map(object.get${element.getName()?cap_first}(), fieldElementRiceType, ricePacket), reference);
            </#if>
        </#if>
    </#list>
    }
</#macro>

<#macro append_list_method rice_type>protected void appendList(<@type_primitive_name rice_type/>objects, RicePacket ricePacket, RiceReference reference){
    if (objects == null){
        appendNULL(reference);
        return;
    }
    appendNOT_NULL(reference);
    for(int i=0;i< objects.length;i++){
        appendNOT_NULL(reference);
        <#if rice_type.getElements()[0].getType().isInstanceOf(list_rice_type)>
            appendList(objects[i], ricePacket, reference);
        <#elseif rice_type.getElements()[0].getType().getMetaType() == 'STRUCT_' >
            appendRicePointer(map(objects[i], typeRegistry.getRiceTypeByRiceTypeName("${code_gen_utils.getListComponentType(rice_type, base_type_registry).getName()}"), ricePacket), reference);
        <#else>
            append<@type_simple_name rice_type.getElements()[0].getType()/>(objects[i], reference);
        </#if>
    }
    appendNULL(reference);
}
</#macro>

<#macro append_enum_method rice_type>protected void append<@type_simple_name rice_type/>(<@type_full_name rice_type/> object, RiceReference reference) {
    if(object == null) {
        appendNULL(reference);
        return;
    }
    appendNOT_NULL(reference);
    switch(object) {
        <#list base_type_registry.getJavaTypeByRiceType(rice_type).getEnumConstants() as enum>
            case ${enum.name()}: reference.append(mapEnumToRiceValue(${enum?index}, ${base_type_registry.getJavaTypeByRiceType(rice_type).getEnumConstants()?size}));
            break;
        </#list>
    }
}
</#macro>

<#macro deserialize_byte_method rice_type>public int deserialize<@type_simple_name rice_type/>(DeserializeContext deserializeContext, int entityIndex) throws ClassNotRegisteredException {
    <@type_full_name rice_type/> aStruct = (<@type_full_name rice_type/>) deserializeContext.getFromHeap(entityIndex);
if(aStruct == null)
aStruct = new  <@type_full_name rice_type/>();

    <#list code_gen_utils.getSuperTypeList(rice_type) as type_or_supertype>
    retrieve<@type_simple_name type_or_supertype/>Fields(aStruct, deserializeContext);
    </#list>
return entityIndex+1;
}
</#macro>

<#macro retrieve_byte_enum_method type>protected <@type_full_name type/> retrieve<@type_simple_name type/>(DeserializeContext deserializeContext){
    if(retrieveIsNull(deserializeContext))
        return null;
    int constraintsQuantity = ${code_gen_utils.getEnumBitCount(code_gen_utils.getEnumClassByRiceType(type, base_type_registry))};
    int constraintNum = retrieveEnumConstraint(deserializeContext, constraintsQuantity);
    <@type_full_name type/> result = <@type_full_name type/>.values()[constraintNum];
    return result;
}
</#macro>


<#macro retrieve_byte_fields_method type>private void retrieve<@type_simple_name type/>Fields(RiceStruct object, DeserializeContext deserializeContext){
if(object==null)
return;
    <@type_full_name type/> realObject = (<@type_full_name type/>) object;
    <#list type.getElements() as element>
        <#if element.getType().getMetaType() != 'INSTRUCTION_'>
            <#if element.getType().isInstanceOf(list_rice_type)>
                realObject.set${element.getName()?cap_first}(retrieve<@type_simple_name element.getType()/>(deserializeContext)); // using ${element.getType().toString()} like list
            <#elseif element.getType().getMetaType() == 'ENUM_' || element.getType().getMetaType() == 'PRIMITIVE_'>
                realObject.set${element.getName()?cap_first}(retrieve<@type_simple_name element.getType()/>(deserializeContext));  // using ${element.getType().toString()} like enum or primitive
            <#elseif element.getType().getMetaType() == 'STRUCT_'>
                realObject.set${element.getName()?cap_first}((<@type_full_name element.getType()/>)retrieve<@type_simple_name element.getType()/>RicePointer(deserializeContext));  // using ${element.getType().toString()} like struct
            <#else>
                //FIXME:: unreacheble generated code statement in 'retrieve_byte_fields_method' macros with type ${element.getType().getName()}
            </#if>
        </#if>
    </#list>
}
</#macro>

<#macro retrieve_byte_list rice_type> private <@type_primitive_name rice_type/> retrieve<@type_simple_name rice_type/>(DeserializeContext deserializeContext){
if(retrieveIsNull(deserializeContext))
return null;
List aListStruct = new ArrayList();
boolean notNull = retrieveIsNotNull(deserializeContext);
while(notNull){
    <#if rice_type.getElements()[0].getType().isInstanceOf(list_rice_type)>
        aListStruct.add(retrieve<@type_simple_name rice_type.getElements()[0].getType()/>(deserializeContext));
    <#elseif rice_type.getElements()[0].getType().getMetaType()=='STRUCT_'>
        aListStruct.add(retrieve<@type_simple_name rice_type.getElements()[0].getType()/>RicePointer(deserializeContext));
    <#else>
        aListStruct.add(retrieve<@type_simple_name rice_type.getElements()[0].getType()/>(deserializeContext));
    </#if>
notNull = retrieveIsNotNull(deserializeContext);
}
    <@type_primitive_name rice_type/> result = new <@type_primitive_name code_gen_utils.getListComponentType(rice_type, base_type_registry)/>[aListStruct.size()]${code_gen_utils.getListArrayBracketsMinusOne(rice_type, base_type_registry)};
    <#if !rice_type.getElements()[0].getType().isInstanceOf(list_rice_type)>
        int i = 0;
        for (Object e : aListStruct)
            result[i++] = (<@type_primitive_name rice_type.getElements()[0].getType()/>)e;
    <#else>
        aListStruct.toArray(result);
    </#if>
return result;
}
</#macro>

<#macro retrieve_byte_rice_pointer element>private RiceStruct retrieve<@type_simple_name element/>RicePointer(DeserializeContext deserializeContext){
if(retrieveIsNull(deserializeContext))
return null;
int address = getAddress(deserializeContext);
int relativeTypeId;
    <#if base_type_registry.getChildrenQuantity(element) != 0 >
    relativeTypeId = retrieveRelativeTypeId(deserializeContext, ${base_type_registry.getChildrenQuantity(element)});
    <#else>
    relativeTypeId = 0;
    </#if>
RiceStruct aStruct = deserializeContext.getFromHeap(address);
if(aStruct == null){
    <#if base_type_registry.getChildrenQuantity(element) != 0 >
        <#list base_type_registry.getChildren(element)>
        switch(relativeTypeId){
        case 0: aStruct = new <@type_full_name element/>();
        break;
            <#items  as children_type>
            case ${children_type?index+1}: aStruct = new <@type_full_name children_type/>();
            break;
            </#items>
        }
        </#list>
    <#else>
    aStruct = new <@type_full_name element/>();
    </#if>
    deserializeContext.putRiceStruct(address, aStruct);
}
return aStruct;
}
</#macro>









